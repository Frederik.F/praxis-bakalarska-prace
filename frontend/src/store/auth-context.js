import React, {useState, useEffect, useCallback, useMemo} from 'react';
import axios from '../hooks/auth-axios';
import nonAuthAxioms from 'axios';
import {isJson} from "../utils/Utils";
import {API_BASE_URL, ROLES_DEFINITION} from "../config";
import {Comparator} from "react-bootstrap-table2-filter";

let logoutTimer;

const AuthContext = React.createContext({
    token: '',
    isLoggedIn: false,
    isAdmin: false,
    userData: {},
    isLoading: false,
    login: (token, expirationTime, userData) => {
    },
    logout: () => {
    },
});

const calculateRemainingTime = expirationTime => {
    const currentTime = new Date().getTime();
    const adjExpirationTime = new Date(expirationTime).getTime();

    return adjExpirationTime - currentTime;
}

const retrieveStoredtoken = (req, res) => {
    const storedToken = localStorage.getItem('token');
    const storedExpirationTime = localStorage.getItem('expirationTime');

    const remainingTime = calculateRemainingTime(storedExpirationTime);

    if (remainingTime <= 3600) {
        localStorage.removeItem('token');
        localStorage.removeItem('expirationTime');
        return null;
    }

    return {
        token: storedToken,
        duration: remainingTime,
    }
}

const getCurrentUser = (WSCOOKIE, userName) => {
    return nonAuthAxioms.get(`${API_BASE_URL}/users/currentUser`, {
        headers: {
            WSCOOKIE,
            schoolURL: localStorage.getItem('selectedSchoolURL')
        }
    });

}

/**
 *
 * @returns {Promise<*[]>}
 *
 * Function that returns all student data from stag
 * @param WSCOOKIE
 * @param schoolURL
 * @param osCislo
 */
const getStudentData = (WSCOOKIE, schoolURL, osCislo) => {
    return nonAuthAxioms.get(`${API_BASE_URL}/students/getInfo?osCislo=${osCislo}`, {
        headers: {
            WSCOOKIE,
            schoolURL
        }
    });
}

/**
 *
 * @param userName
 * @param getOnlyCurrent
 * @returns {Promise<*[]>}
 *
 * Function that get students signed courses and find not completed internship courses
 */
const getCoursesThatAreNotAbsolved = async ({userName}, getOnlyCurrent = false) => {
    const courses = await axios.get(`./students/getCourses?stagUser=${userName}`);
    let matchedCourses = [];

    if (courses.data) {
        const localCourses = await axios.get(`./course`, {
            params: {
                filter: {
                    fieldName: 'active',
                    filterVal: 'a',
                    comparator: Comparator.EQ
                }
            }
        });

        return localCourses.data;

        localCourses.data.forEach(localCourse => {
            const matchedCourse = courses.data.reverse().find(course => (course.zkratka.toLowerCase() === localCourse.name.toLowerCase() && course.absolvoval !== "A" && (!getOnlyCurrent || (getOnlyCurrent && (new Date().getFullYear() - course.rok) < 1))));
            const {
                id, active, approximate_days_for_completion,
                department, name,
            } = localCourse;
            if (!!matchedCourse) matchedCourses.push({
                id, active, approximate_days_for_completion,
                department, name,
            });
        });
    }


    return matchedCourses;
};

export const AuthContextProvider = ({children}) => {
    const tokenData = useMemo(retrieveStoredtoken,[]);

    let initToken;
    if (tokenData) {
        initToken = tokenData.token;
    }

    const [token, setToken] = useState(initToken);
    const [userData, setUserData] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const userIsLoggedIn = !!token && userData.hasOwnProperty('userName');
    const isUserAdmin = ROLES_DEFINITION.admin.includes(userData.role);

    const logoutHandler = useCallback(async () => {
        await axios.get(`./users/logout`);
        setToken(null);
        localStorage.removeItem('token');
        localStorage.removeItem('expirationTime');

        if (logoutTimer) {
            clearTimeout(logoutTimer);
        }
    }, []);

    const loginHandler = async (token, expirationTime, userData = null) => {
        setToken(token);
        let usData = '';
        if (isJson(userData)) {
            usData = JSON.parse(userData).stagUserInfo[0];
        }

        localStorage.setItem('token', token);
        localStorage.setItem('expirationTime', expirationTime);

        // if user is student get student info
        let studentInfo = {};
        if (usData.role === 'ST') {
            setIsLoading(true);
            studentInfo = await getStudentData(token, localStorage.getItem('selectedSchoolURL'), usData.userName);
            studentInfo = studentInfo.data;
            studentInfo.courses = await getCoursesThatAreNotAbsolved(usData);
        }

        setUserData({...usData, ...studentInfo})
        const remainingTime = calculateRemainingTime(expirationTime);
        logoutTimer = setTimeout(logoutHandler, remainingTime);
        setIsLoading(false);
    };

    useEffect(() => {
        if (tokenData?.token) {
            setIsLoading(true);
            getCurrentUser(tokenData.token, tokenData.userName).then(response => {
                const user = response.data;
                if (user.role === 'ST') {
                    new Promise(async (resolve, reject) => {
                        let studentInfo = await getStudentData(tokenData.token, localStorage.getItem('selectedSchoolURL'), user.userName);
                        studentInfo = studentInfo.data;
                        studentInfo.courses = await getCoursesThatAreNotAbsolved(user);
                        resolve(studentInfo);
                    }).then(r => {
                        setUserData({...user, ...r});
                        setIsLoading(false);
                    });

                }else{
                    setIsLoading(false);
                    setUserData(user);
                }
            }).catch(e => {
                logoutHandler().then(r => window.location.reload());
                console.log('error', e.response);
            })
            logoutTimer = setTimeout(logoutHandler, tokenData.duration);
        }
    }, [tokenData, logoutHandler])


    const contextValue = {
        token,
        userData,
        isLoggedIn: userIsLoggedIn,
        login: loginHandler,
        logout: logoutHandler,
        isAdmin: isUserAdmin,
        isLoading
    }

    return <AuthContext.Provider value={contextValue}>
        {children}
    </AuthContext.Provider>;
}

export default AuthContext;