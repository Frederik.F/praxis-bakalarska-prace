export const API_BASE_URL = 'http://localhost:3331/API/v1';

export const SCHOOLS_LIST = [
    {
        label: 'IS/STAG DEMO',
        value: 'https://stag-demo.zcu.cz/',
        isDev: true
    },
    {
        label: 'UJEP | Univerzita J. E. Purkyně',
        value: 'https://ws.ujep.cz/'
    },
    {
        label: 'Západočeská univerzita v Plzni',
        value: 'https://stag-ws.zcu.cz/'
    },
]

// Date UI format
export const dateFormat = 'DD.MM.yyyy';
export const dateTimeFormat = 'DD.MM.yyyy hh:mm:ss';

//Validation patterns
export const emailPattern = '/^(([^<>()[\\]\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})$/i;';

export const ROLES_DEFINITION = {
    admin: [
        'AD',
        'PX',
        'OP'
    ],
};