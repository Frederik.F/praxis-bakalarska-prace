import PizZipUtils from "pizzip/utils";
import PizZip from "pizzip";
import Docxtemplater from "docxtemplater";
import {saveAs} from "file-saver";
import moment from "moment";
import {dateFormat, dateTimeFormat} from "../config";
import nonAuthAxioms from "axios";
import axios from "../hooks/auth-axios";

export const encodeQueryData = (data) => {
    const ret = [];
    for (let d in data)
        if (d && data[d]) {
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(typeof data[d] === "object" ? JSON.stringify(data[d]) : data[d]));
        }

    return ret.join('&');
}


export function number_format(number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    const n = !isFinite(+number) ? 0 : +number;
    const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
    const sep = typeof thousandsSep === 'undefined' ? ' ' : thousandsSep;
    const dec = typeof decPoint === 'undefined' ? ',' : decPoint;

    const toFixedFix = function toFixedFix(n, prec) {
        const k = Math.pow(10, prec);
        return '' + (Math.round(n * k) / k).toFixed(prec);
    };

    const s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');

    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }

    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }

    return s.join(dec);
}

export function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

export const isPublicHoliday = async date => {
    return await nonAuthAxioms.get(`https://svatky.vanio.cz/api/${formatDateDb(date)}`)?.isPublicHoliday;

}

export const generateUniqId = () => {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

export const formatDate = (date, f = dateFormat) => {
    return moment(date).format(f)
};

export const formatDateTime = (datetime, f = dateTimeFormat) => {
    return moment(datetime).format(f);
};
export const formatDateDb = date => {
    return moment(date).format('YYYY-MM-DD')
};

export const formatDateTimeDb = datetime => {
    return moment(datetime).format('YYYY-MM-DD hh:mm:ss');
};

export const todayPercentageValueBetweenDates = (startDate, endDate) => {
    const today = moment().unix()
    const q = Math.abs(today - moment(startDate).unix());
    const d = Math.abs(moment(endDate).unix() - moment(startDate).unix());
    return Math.round((q / d) * 100)
}

export const getDaysBetweenDates = (startDate, endDate) => {
    let date_1 = typeof startDate === "string" ? new Date(startDate) : startDate;
    let date_2 = typeof endDate === "string" ? new Date(endDate) : endDate;

    let difference = date_2.getTime() - date_1.getTime();
    return Math.abs(Math.ceil(difference / (1000 * 3600 * 24)));
}

export const getBusinessDays = (startDate, endDate, returnAsCount = true, startsWithMonday = false, exlcudeDays = []) => {
    const day = moment(startDate);
    const businessDays = [];

    while (day.isSameOrBefore(endDate, 'day')) {

        const dayNum = day.day();

        if (!startsWithMonday) {
            if (dayNum !== 0 && dayNum !== 6 && !exlcudeDays.includes(dayNum)) {
                businessDays.push(formatDateDb(day));
            }
        } else {
            if (dayNum !== 1 && dayNum !== 6 && !exlcudeDays.includes(dayNum)) {
                businessDays.push(formatDateDb(day));
            }
        }

        day.add(1, 'd');
    }

    return returnAsCount ? businessDays.length : businessDays;
}

export const getPublicHolidaysInterval = async (startDate, endDate, returnAsCount = true) => {
    const businessDays = getBusinessDays(startDate, endDate, false);
    const publicHolidays = [];
    await new Promise((resolve, reject) => {
        businessDays.forEach((day, idx, array) => {
            nonAuthAxioms.get(`https://svatky.vanio.cz/api/${day}`).then((response) => {
                const data = response.data;
                if (data.isPublicHoliday) {
                    publicHolidays.push(data);
                }

                if (idx === array.length - 1) {
                    resolve(true);
                }
            });
        })
    })

    publicHolidays.sort((a, b) => new Date(a.date) - new Date(b.date));
    return returnAsCount ? publicHolidays.length : publicHolidays;
}

export const addSelectedRowClass = (element, unselectAll = false) => {
    const row = element.closest('tr');
    const tbody = element.closest('tbody');
    const allSelectedRows = tbody.querySelectorAll('tr.selected');
    allSelectedRows.forEach((row) => row.classList.remove("selected"));
    if (!unselectAll) {
        row.classList.add('selected')
    }
}

export const handleCURD = (table, id, data, idAsParam = false, action = 'patch') => {
    if (!!id && !!data) {
        let url =`./${table}`;
        if(idAsParam){
            url += '/'+id;
        }

        return axios[action](url, {
            id,
            data
        })
    }
    return null;

};

export const handleTableInsert = (table, data) => {
    if (!!(data)) {
        return axios.post(`./${table}`, {
            data
        });
    }
    return null;

};

export const loadFile = (url, callback) => {
    PizZipUtils.getBinaryContent(url, callback);
};

export const generateDocumentFromTemplate = (url, variables, outputName = 'output') => {
    const replaceVariablesAndReturnDocument = (error, content) => {
        if (error) {
            throw error;
        }

        const zip = new PizZip(content);
        const doc = new Docxtemplater(zip, {
            paragraphLoop: true,
            linebreaks: true,
        });

        doc.render(variables);
        //console.log('docVariables',variables);

        const out = doc.getZip().generate({
            type: "blob",
            mimeType:
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        }); //Output the document using Data-URI

        saveAs(out, `${outputName || 'output'}.docx`);
    }

    let fileUrl = url;
    if(!url.startsWith('blob')){
        try {
            fileUrl = require(`../templates/${url}`)?.default
        } catch (err) {
            console.error(`${url} template not found!`)
            //errorHandler('Docx šablona nebyla nalezena!');
            return null;
        }
    }
    if (fileUrl)
        PizZipUtils.getBinaryContent(fileUrl, replaceVariablesAndReturnDocument);
}

/*export const setColorVariant = (theme, variant) => {
    const path = getColorVariantPath(variant);
    document.getElementById('color-palette').setAttribute('href', path);
    if (!variant || variant === 'default') {
        localStorage.removeItem('color-palette');
    } else {
        localStorage.setItem('color-palette', variant);
    }
}*/

export const getTouchedField = (origin, actual) => {
    let touchedFields = {};
    Object.entries(actual).forEach(([key, val]) => {
        if (origin[key] !== val) { // ignore types
            touchedFields[key] = actual[key];
        }
    });

    return touchedFields;
}

export const generateFileFromTableAPI = (templateId, data, outputName, simpleDownload = false) => {
    return axios.get(`/template/file/${templateId}`, {
        responseType: 'blob', // important
    }).then((response) => {
        const url = window.URL.createObjectURL(response.data);
        if(simpleDownload){
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', `${outputName}.docx`); //or any other extension
            document.body.appendChild(link);
            link.click();
        }else{
            generateDocumentFromTemplate(url, data, outputName);
        }
    })
}

export const downloadFileFromTableAPI = (templateId, outputName) => {
    return generateFileFromTableAPI(templateId, null, outputName, true);
}

export const getColorVariantPath = (variant) => variant ? `styles/themes/light-color-variants/${variant}.css` : '';