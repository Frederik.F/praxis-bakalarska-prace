import axios from 'axios';
import {API_BASE_URL} from "../config";

// axios instance for making requests
const axiosInstance = axios.create({

});

// request interceptor for adding token
axiosInstance.interceptors.request.use((config) => {
    // add token to request headers
    config.headers['WSCOOKIE'] = localStorage.getItem('token');
    config.headers['schoolURL'] = localStorage.getItem('selectedSchoolURL')
    config.baseURL = API_BASE_URL;
    return config;
});

axiosInstance.defaults.baseURL = API_BASE_URL;

export default axiosInstance;