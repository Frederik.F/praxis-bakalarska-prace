import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';

import 'react-toastify/dist/ReactToastify.css';
import './styles/global.css';
import './styles/themes/basic-colors-light.css'

import React, {useContext} from 'react';
import AuthContext from "./store/auth-context";
import {Route, Switch, Redirect} from 'react-router-dom';
import Navbar from "./components/Navbar";
import StudentHome from "./pages/Student/StudentHome";
import Templates from "./pages/Admin/Templates";
import Welcome from "./pages/Welcome";
import AuthPage from "./pages/AuthPage";
import AdminHome from "./pages/Admin/AdminHome";
import LoginProgress from "./pages/LoginProgress";
import IntershipDetail from "./pages/IntershipDetail";
import Courses from "./pages/Admin/Courses";
import {ToastContainer, Zoom} from "react-toastify";


function App() {
    const authCtx = useContext(AuthContext);

    return (
        <div className={"App"}>
            <div className="main-background"/>
            <Navbar/>
            <ToastContainer
                transition={Zoom}
                pauseOnFocusLoss={false}
            />
            <Switch>
                {authCtx.isLoading ? <LoginProgress /> : null}
                {!authCtx.isLoggedIn ? <AuthPage /> : null}
                <Route path='/Welcome' component={Welcome}/>
                {/*!authCtx.isLoggedIn && <Route path='/Prihlaseni' component={AuthPage}/>*/}

                {/* USET HAS TO BE LOGGED */}
                        {authCtx.isLoggedIn && <Route path='/Detail-praxe/:id' component={IntershipDetail}/>}

                        {/* ADMIN */}
                        {authCtx.isLoggedIn && authCtx.isAdmin && <Route path='/' component={AdminHome} exact/>}
                        {authCtx.isLoggedIn && authCtx.isAdmin && <Route path='/Sablony/:name?' component={Templates}/>}
                        {authCtx.isLoggedIn && authCtx.isAdmin && <Route path='/Kurzy/:name?' component={Courses}/>}
                        {/* END ADMIN */}

                        {/* STUDENT */}
                        {authCtx.isLoggedIn && !authCtx.isAdmin &&  <Route path='/' component={StudentHome} exact/>}
                        {/* END STUDENT */}

                <Route path='*'>
                    {authCtx.isLoggedIn ? <Redirect to='/'/> : <Redirect to='/Prihlaseni'/>}
                </Route>
            </Switch>
        </div>
    );
}

export default App;
