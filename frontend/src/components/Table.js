import React, {useCallback, useState, useMemo, useEffect} from 'react';
import axios from '../hooks/auth-axios';
import T from "prop-types";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, {PaginationProvider, PaginationListStandalone} from 'react-bootstrap-table2-paginator';
import filterFactory from 'react-bootstrap-table2-filter';
import {formatDate, formatDateDb, formatDateTime} from "../utils/Utils";
import {Col, Container, Row, Form} from "react-bootstrap";
import {number_format} from "../utils/Utils";

export const TEXT_FIELD = 'TEXT';
export const NUMBER_FIELD = 'NUMBER';
export const DATE_FIELD = 'DATE';
export const DATE_TIME_FIELD = 'DATE_TIME';
export const CUSTOM_FIELD = 'CUSTOM';
export const PHONE_FIELD = 'PHONE_NUMBER';
export const EMAIL_FIELD = 'EMAIL';

const TYPE_PROPS = ({
    [NUMBER_FIELD]: ({
        formatter: (cell) => number_format(cell),
        align: 'right'
    }),
    [DATE_FIELD]: ({
        formatter: (cell) => cell ? formatDate(cell) : cell
    }),
    [DATE_TIME_FIELD]: ({
        formatter: (cell) => cell ? formatDateTime(cell) : cell
    }),
});

export const getTypeProps = type => TYPE_PROPS[type];

/*const getKey = (url, pageIndex, rowsPerPage, sort, filter) => {
    const checkedUrl = url.endsWith('?') ? url : `${url}?`;
    const query = encodeQueryData({
        rowsPerPage,
        pageIndex,
        sort,
        filter
    });

    return `${checkedUrl}${query}`;
}*/

export const Table = props => {
    const {
        API, rowEvents, keyField, className, columnsDef, showPaginator, paginationOption,
        loading, remote, topComponent, strictPerSize, hideTotalNumberRowsNumber, noDataLabel, defaultOrderBy, pushedFilters,
        id, onDidMount
    } = props;

    const pageStartIndex = 0;

    const minRowsPerPage = 10;
    const [rowsPerPage, setRowsPerPage] = useState(strictPerSize || minRowsPerPage);
    const [pageIndex, setPageIndex] = useState(pageStartIndex);
    const [didMount, setDidMount] = useState(false);

    const [orderBy, setOrderBy] = useState(defaultOrderBy);
    const [filter, setFilter] = useState(pushedFilters);
    const [data, setData] = useState([]);
    const [isValidating, setIsValidating] = useState(false);

    const {resolvedColsDef, relatedFields, fullRelatedField} = useMemo(() => {
        const relatedFields = {};
        const fullRelatedField = {};

        const resolvedColsDef = columnsDef.map(originalDef => {
            const baseTypeProps = getTypeProps(originalDef.fieldType) || {};
            if(originalDef.related){
                relatedFields[originalDef.dataField] = originalDef.related;
                fullRelatedField[originalDef.dataField] = originalDef;
            }

            return {
                ...baseTypeProps,
                ...originalDef
            };
        });
        return {
            resolvedColsDef,
            relatedFields,
            fullRelatedField
        }

    },[columnsDef]);


    /*const {
        data,
        isValidating
    } = useSWR(getKey(`http://localhost:3331/api/v1/${API}`, pageIndex, rowsPerPage, sort, filter))*/

    useEffect(() => {
        if(!didMount && !Array.isArray(data) && typeof onDidMount === "function"){
            onDidMount(data);
            setDidMount(true);
        }
    },[data]);

    useEffect(() => {
        const timer = setTimeout(() => setIsValidating(true), 500);
        let api = API.split('/');
        axios.get(`./${api[0]}/table${api[1] ? '/'+api[1] : ''}`,{
            params:{
                orderBy: JSON.stringify(orderBy),
                filter: JSON.stringify(filter),
                include: Object.keys(relatedFields).length > 0 ? JSON.stringify(relatedFields) : null,
                skip: (pageIndex === pageStartIndex ? 0 : pageIndex) * rowsPerPage,
                take: rowsPerPage
            },

        }).then(({data}) => {
            setData(data);
            clearTimeout(timer);
            setIsValidating(false);
        }).catch(e => {
            clearTimeout(timer);
            setIsValidating(false);
        })
    }, [API, relatedFields, pageIndex, rowsPerPage, orderBy, filter])


    const paginatorOptions = {
        custom: true,
        totalSize: data?.totalRows,
        sizePerPage: rowsPerPage,
        pageStartIndex: pageStartIndex,
        prePageTitle: 'Předchozí stránka',
        nextPageTitle: 'Další stránka',
        firstPageTitle: 'První stránka',
        lastPageTitle: 'Poslední stránka',
        ...paginationOption
    }

    const onTableChange = useCallback((type, newState) => {
        if (type === "pagination") {
            setPageIndex(newState.page)
        } else if (type === "sort") {
            setOrderBy({
                [newState.sortField]: newState.sortOrder
            })
        } else if (type === "filter") {
            const finalFilter = [];
            Object.entries(newState.filters).forEach(([fieldName, filters]) => {
                let {filterVal, comparator} = filters;
                if (filters.filterType === 'DATE') {
                    filterVal = formatDateDb(filterVal.date);
                }


                if (filterVal && filterVal.toLowerCase() !== "invalid date") {
                    const f = {
                        fieldName,
                        filterVal,
                        comparator,
                        fieldType: filters.filterType,
                    };
                    if (fullRelatedField[fieldName]?.filterReplaceName) {
                        f.filterReplaceName = fullRelatedField[fieldName].filterReplaceName;
                    }

                    if (fullRelatedField[fieldName]?.dataType) {
                        f.dataType = fullRelatedField[fieldName].dataType;
                    }

                    finalFilter.push(f);
                }
            });

            if(!!pushedFilters && pushedFilters.length > 0){
                finalFilter.push(...pushedFilters);
            }

            setFilter(finalFilter.filter(col => col.filterVal !== null));

            setPageIndex(0);
        }
    }, [pushedFilters, fullRelatedField]);

    const rowClasses = (row, rowIndex) => {
        return rowEvents ? 'evented ' : '';
    };

    const tableIsLoading = (isValidating || loading);

    const perPageOptions = useMemo(() => [10, 20, 50, 100, 500], []);

    const c = typeof className === "string" ? className.split(" ") : className;
    const classes = [tableIsLoading ? 'commonLoadingSpinner' : '', ...c];


    const isPaginatorVisible = (showPaginator && data?.totalRows > minRowsPerPage);

    const TOPCompo = () => <Container className="table-actions-containert">
        {typeof topComponent === 'function' ? topComponent() : topComponent}
    </Container>

    return <Container className="table-component-holder" >
        <PaginationProvider
            pagination={paginationFactory(paginatorOptions)}>
            {({paginationProps, paginationTableProps}) => {
                return <div>
                    {TOPCompo() &&
                        <Container className="pagination-container">
                            <Row>
                                {TOPCompo()}
                                {isPaginatorVisible && <Col className="pagination-show-per-page">
                                    {!strictPerSize && <>
                                        Ukázat
                                        <Form.Select onChange={e => {
                                            setRowsPerPage(e.target.value);
                                            // set selected page to first page
                                            paginationProps.onPageChange(pageStartIndex);
                                        }}>
                                            {perPageOptions.map(option => <option key={option}>
                                                    {option}
                                                </option>
                                            )}
                                        </Form.Select>
                                    </>}

                                    {!hideTotalNumberRowsNumber &&
                                        <>
                                            {!strictPerSize ? 'záznamů z ' : 'Počet záznamů: '}
                                            {number_format(data?.totalRows || (data?.rows?.length || data?.length))}
                                        </>}
                                </Col>}
                                <Col>
                                    {(isPaginatorVisible && parseInt(data?.totalRows) > rowsPerPage) &&
                                        <PaginationListStandalone
                                            {...paginationProps}
                                        />}
                                </Col>
                            </Row>
                        </Container>
                    }
                    <BootstrapTable
                        striped bordered hover bootstrap4
                        id={id}
                        keyField={keyField}
                        classes={classes.join(' ')}
                        noDataIndication={tableIsLoading ? "Načítání..." : noDataLabel}
                        data={data ? (data?.rows || data) : []}
                        columns={resolvedColsDef}
                        onTableChange={onTableChange}
                        filter={filterFactory()}
                        remote={remote}
                        rowEvents={rowEvents}
                        rowClasses={rowClasses}
                        {...paginationTableProps}
                    />

                </div>
            }
            }
        </PaginationProvider>
    </Container>
}

Table.propTypes = {
    keyField: T.string.isRequired,
    columnsDef: T.array.isRequired,
    className: T.oneOfType([T.array, T.string]),
    showPaginator: T.bool,
    paginationOption: T.object,
    remote: T.object,
    rowEvents: T.object,
    API: T.string,
    topComponent: T.oneOfType([T.node, T.func]),
    strictPerSize: T.oneOfType([T.string, T.number]),
    hideTotalNumberRowsNumber: T.bool,
    noDataLabel: T.string,
    defaultOrderBy: T.oneOfType([T.object, T.array]),
    pushedFilters: T.arrayOf(T.shape({
        fieldName: T.string.isRequired,
        filterVal: T.string.isRequired,
        comparator: T.string.isRequired
    })),
    id: T.string,
    onDidMount: T.func
};

Table.defaultProps = {
    className: [],
    showPaginator: true,
    rowEvents: null,
    topComponent: null,
    strictPerSize: null,
    hideTotalNumberRowsNumber: false,
    noDataLabel: 'Žádná data',
    remote: {
        filter: true,
        pagination: true,
        sort: true,
    },
    defaultOrderBy: null,
    pushedFilters: null,
    id: null,
    onDidMount: null
};

export default Table;