import React, {useRef, useCallback} from 'react';
import Docxtemplater from 'docxtemplater';
import PizZip from "pizzip";
import PizZipUtils from 'pizzip/utils/index';
import {saveAs} from "file-saver";
import {Button} from "react-bootstrap";
import T from 'prop-types';
import DocumentViever from "./Document-viever";

const ContractGenerator = props => {
    const {url, docVariables, asFileInput, localDocxName, errorHandler, outputName, showPreview, disabled} = props;
    const fileRef = useRef();
    const loadFile = useCallback((url, callback) => {
        PizZipUtils.getBinaryContent(url, callback);
    }, []);

    const replaceVariablesAndReturnDocument = useCallback((error, content) => {
        if (error) {
            throw error;
        }

        const zip = new PizZip(content);
        const doc = new Docxtemplater(zip, {
            paragraphLoop: true,
            linebreaks: true,
        });

        doc.render(docVariables);
        //console.log('docVariables',docVariables);

        const out = doc.getZip().generate({
            type: "blob",
            mimeType:
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        }); //Output the document using Data-URI

        saveAs(out, `${outputName || 'output'}.docx`);
    }, [outputName]);

    const generateDocument = useCallback(() => {
        let docx = url;
        if(!docx){
            try {
                docx = require(`../templates/${localDocxName}`)?.default
            } catch (err) {
                console.error('Docx template not found!')
                errorHandler('Docx šablona nebyla nalezena!');
                return null;
            }
        }
        loadFile(
            docx,
            replaceVariablesAndReturnDocument
        );

    }, [url, localDocxName, asFileInput]);

    const generate = () => {
        const docs = fileRef.current;
        const reader = new FileReader();
        if (docs.files.length === 0) {
            errorHandler("Nebyl vybrán žádný soubor!");
            return false;
        }

        reader.readAsBinaryString(docs.files.item(0));

        reader.onerror = (evt) => {
            console.log("error reading file", evt);
            errorHandler('Chyba při čtení souboru');
        };

        reader.onload = (evt) => {
            const content = evt.target.result;
            replaceVariablesAndReturnDocument(null, content);
        };
    }

    if(!url && !localDocxName && !asFileInput){
        return 'Contract-editr.js: Url, localDoxName or asFileInput was not declared';
    }

    return <>
        {showPreview && <DocumentViever />}
        {asFileInput ? <>
                <input
                    type="file"
                    ref={fileRef}
                    accept= ".docx, application/msword"
                />
                <Button onClick={generate} disabled={disabled}>
                    Generovat
                </Button>
            </> :
            <Button onClick={generateDocument} disabled={disabled}>
                Vygenerovat Dokument
            </Button>
        }
    </>;
}

const atLeasOneIsRequred = (props, propName, componentName) => {
    if (props.url && typeof props.url !== "string") {
        return new Error(`url has to be string`);
    }

    if (props.localDocxName && typeof props.localDocxName !== "string") {
        return new Error(`localDocxName has to be string.`);
    }

    if (props.asFileInput && typeof props.asFileInput !== "boolean") {
        return new Error(`asFileInput has to be boolean.`);
    }

    if (!props.url && !props.localDocxName && !props.asFileInput) {
        return new Error(`One of props 'url', 'localDocxName' or asFileInput was not specified in '${componentName}'.`);
    }
}

ContractGenerator.propTypes = {
    docVariables: T.object.isRequired,
    url: atLeasOneIsRequred,
    localDocxName: atLeasOneIsRequred,
    asFileInput: atLeasOneIsRequred,
    errorHandler: T.func,
    outputName: T.string,
    showPreview: T.bool,
    disabled: T.bool
};

ContractGenerator.defaultProps = {
    asFileInput: false,
    showPreview: false,
    disabled: false,
    errorHandler: () => {}
};

export default ContractGenerator;

