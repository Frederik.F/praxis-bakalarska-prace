import React from "react";
import T from "prop-types";
import {Alert, Modal} from "react-bootstrap";


export const SimplePopUpAlert = ({show, onHide, variant, heading, message, classNameAppend}) => {

    return (
        <>
            <Modal show={show} onHide={onHide} className={["simple-alert-modal", (classNameAppend)].join(' ')}>
                {heading && <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>}
                {!!variant ? <Alert variant={variant}>
                    {message}
                </Alert> : <Modal.Body>
                    {message}
                </Modal.Body>}
            </Modal>
        </>
    );

};

SimplePopUpAlert.propTypes = {
    show: T.bool.isRequired,
    onHide: T.func,
    heading: T.string,
    message: T.string,
    classNameAppend: T.string
}

SimplePopUpAlert.defaultProps = {
    onHide: null,
    heading: null,
    message: null,
    classNameAppend: ''
}


export default SimplePopUpAlert;
