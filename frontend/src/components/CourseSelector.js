import React, {useContext} from 'react';
import T from "prop-types";
import {Container} from "react-bootstrap";
import HeadLineSeparator from "./Head-line-separator";
import AuthContext from "../store/auth-context";
import Button from "react-bootstrap/Button";


export const CourseSelector = ({selectCourse}) => {
    const authCtx = useContext(AuthContext);

    const {userData} = authCtx;
    const {courses} = userData;

    return <Container className="main-container md">
        <HeadLineSeparator text="Výber kurzu"/>
        <div className="d-flex justify-content-center">
            Vyberte kurz, pro který si přejete vytvořit praxi
        </div>
        <Container className="d-flex justify-content-center align-content-stretch flex-wrap" style={{marginTop: 10}}>
            {courses.map(course => <Button className="course-select-button-container" key={course.id}
                                           onClick={() => selectCourse(course)}>
                {course.department && course.department + '/' + course.name}
            </Button>)}
        </Container>
    </Container>
};


CourseSelector.propTypes = {
    selectCourse: T.func
};

CourseSelector.defaultProps = {};

export default CourseSelector;
