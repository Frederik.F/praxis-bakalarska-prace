import React, {Fragment} from 'react';
import Icon from "./Icon";
import {Container} from "react-bootstrap";

const CirclePart = ({percentage = 100, icon = null, description = null}) => {
    const iconStart = 37;
    const iconEnd = 64;
    let iconPercentage = 0;

    if (percentage > iconStart) {
        const q = Math.abs(percentage - iconStart);
        const d = Math.abs(iconEnd - iconStart);
        iconPercentage = Math.round((q / d) * 100);
    }

    return <Container className="part-container">
        <div className="circle-part outer-black-border"/>
        <div className="circle-part colored-part"
             style={{backgroundImage: `linear-gradient(white, white), linear-gradient(to right, var(--primary-color) ${percentage}%, rgb(255, 255, 255) 0%)`}}>
            {icon &&
                <Icon icon={icon}
                      className="center-icon"
                      style={{background: `linear-gradient(to right, var(--primary-color) ${iconPercentage}%, #ffffff 0%)`}}/>
            }
        </div>
        {description &&
            <div className="description-text">
                {description}
            </div>}
    </Container>
};

const ArrowRight = props => {
    return <div className="arrow-right">
        <Icon icon="arrow-right"/>
    </div>;
}

export const StateDiagram = ({states}) => {

    return <div className="state-diagram">
        {states.map((state, i) => {
            return <Fragment key={i}>
                <CirclePart icon={state.icon} percentage={state.percentage} description={state.description}/>
                {!((states.length === 1 || i === states.length - 1)) && <ArrowRight/>}
            </Fragment>;
        })}
    </div>;
};

export default StateDiagram;