import {Overlay, Popover} from "react-bootstrap";
import React from "react";

const SimpleOverlay = ({setShow, show, target, header, style = null, bodyClassName = null, children}) => {
    return <Overlay
        rootClose
        onHide={() => setShow(false)}
        show={show}
        containerPadding={20}
        target={target}
    >
        <Popover id="popover-contained" style={style}>
            {header && <Popover.Header as="h3">{header}</Popover.Header>}
            <Popover.Body className={bodyClassName}>
                {children}
            </Popover.Body>
        </Popover>
    </Overlay>
}

export default SimpleOverlay;