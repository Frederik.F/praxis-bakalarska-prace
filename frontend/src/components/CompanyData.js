import React, {useCallback, useContext, useRef, useState, useEffect} from 'react';
import T from "prop-types";
import {Alert, Col, Form, Row} from "react-bootstrap";
import HeadLineSeparator from "./Head-line-separator";
import nonAuthAxioms from 'axios';
import BInputLabeled from "./BInputLabeled";
import Button from "react-bootstrap/Button";
import Icon from "./Icon";
import AuthContext from "../store/auth-context";
import {
    getBusinessDays,
    formatDate,
    getPublicHolidaysInterval,
    formatDateDb, generateFileFromTableAPI
} from "../utils/Utils";
import {DATE_FIELD, EMAIL_FIELD, PHONE_FIELD} from "./Table";
import axios from "../hooks/auth-axios";
import {useHistory} from "react-router-dom";
import AutoRow from "./AutoRow";
import SimplePopUpAlert from "./SimplePopupAlert";
import AsyncSelect from "react-select/async";
import {components} from 'react-select';
import SimpleOverlay from "./SimpleOverlay";
import Dialog from 'react-bootstrap-dialog'
import {Comparator} from "react-bootstrap-table2-filter";
import {API_BASE_URL} from "../config";
import {toast} from "react-toastify";


export const CompanyData = props => {
    const authCtx = useContext(AuthContext);
    const history = useHistory();

    let {course} = props;

    const [validated, setValidated] = useState(null);

    const [error, setError] = useState('');
    const [companyData, setCompanyData] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [lastUsedIco, setLastUsedIco] = useState(null);
    const [showSuccessAlert, setShowSuccessAlert] = useState(false);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [input, setInput] = useState("");
    const input_ico_ref = useRef();
    const [publicHolidays, setPublicHolidays] = useState(0);

    const [contracts, setContracts] = useState(props.contracts);

    const [show, setShow] = useState(false);
    const target = useRef(null);

    const confirmDialog = useRef();

    const internshipToRegenerate = props.internship;
    const userData = authCtx.userData;

    const businessDays = getBusinessDays(startDate, endDate);

    const createProperAdress = cData => {
        let countryName = cData.AA.NS !== 'Česká republika' ? (cData.AA.NS || '') : '';

        return `${cData.AD.UC || ''}${cData.AA.CO ? ('/' + cData.AA.CO) : ''}, ${cData.AA.NCO || ''}, ${cData.AD.PB || ''} ${countryName}`;
    }

    const icoEnteredHandle = useCallback(async (ico) => {
        const inputIco = ico;
        if (lastUsedIco === inputIco) return false;

        if (!inputIco) {
            setError('');
            setCompanyData('');
            setIsLoading(false);
            setLastUsedIco(null);
            return false;
        }

        setIsLoading(true);

        const aresData = await nonAuthAxioms.get(`${API_BASE_URL}/ares/${ico}`);
        setLastUsedIco(ico);

        if (aresData.status === 204) {
            setError('Firma nenalezena');
            setIsLoading(false);
            setCompanyData(null);
            return false;
        }

        setCompanyData(aresData.data);
        setError('');
        setIsLoading(false);
    }, [lastUsedIco]);

    useEffect(() => {
        if (internshipToRegenerate) {
            icoEnteredHandle(internshipToRegenerate.ico);
            setStartDate(new Date(internshipToRegenerate.start_date));
            setEndDate(new Date(internshipToRegenerate.end_date));
        }
    }, [icoEnteredHandle, internshipToRegenerate])

    useEffect(() => {
        if (startDate && endDate) {
            getPublicHolidaysInterval(startDate, endDate, false).then(response => {
                setPublicHolidays(response);
            });
        }
    }, [startDate, endDate])

    useEffect(() => {
        if (!contracts) {
            axios.get(`./course/table/includeTemplates`, {
                params: {
                    filter: [{
                        fieldName: 'id',
                        filterVal: course.id,
                        comparator: Comparator.EQ,
                    }]
                }
            }).then(response => {
                setContracts(response.data.rows[0].Template.filter(template => template.type === 'c'));
            });
        }
    }, [course])


    const handleSubmit = useCallback(async e => {
        const form = e.currentTarget;

        e.preventDefault();
        if (form.checkValidity() === false) {
            e.stopPropagation();
            setValidated(true);
            return true;
        }

        setValidated(false);

        let enoughDays = businessDays >= course.approximate_days_for_completion || !!internshipToRegenerate;

        if (!enoughDays && !internshipToRegenerate) enoughDays = await new Promise((resolve, reject) => {
            confirmDialog.current.show({
                title: 'Upozornění',
                body: `Přibližná doba pro splnění kurzu je ${course.approximate_days_for_completion} dnů. Váš interval má ${businessDays} pracovních dní. Přejete si pokračovat?`,
                actions: [
                    Dialog.Action('Vrátit se zpět', () => resolve(false)),
                    Dialog.Action('Pokračovat', () => resolve(true), 'btn-primary')
                ],
                bsSize: 'small',
                onHide: (dialog) => {
                    dialog.hide()
                    resolve(false);
                }
            });
        });


        if (enoughDays) {
            const submittedData = Object.fromEntries(new FormData(e.target));
            //TODO validate submitted Data with template varaibles

            const splittedBirthDate = submittedData.student_datum_narozeni.split('.');
            submittedData.student_den_narozeni = splittedBirthDate[0];
            submittedData.student_mesic_narozeni = splittedBirthDate[1];
            submittedData.student_rok_narozeni = splittedBirthDate[2];
            submittedData.praxe_zacatek = formatDateDb(startDate);
            submittedData.praxe_konec = formatDateDb(endDate);

            const downloadingPromises = [];
            contracts.forEach((contract, i) => {
                const {id, name} = contract;
                const outputName = `${userData.prijmeni}_${name}_${course.name}`;

                if (id) {
                    downloadingPromises.push(generateFileFromTableAPI(id, submittedData, outputName));
                }
            });

            const a = (callback) => Promise.all(downloadingPromises).then(callback);

            if (!!internshipToRegenerate) {
                a(setTimeout(() => history.replace(`/detail-praxe/${internshipToRegenerate.id}`), 1000));
                toast.promise(a, {
                    pending: 'Generování dokumentů',
                    success: 'Dokumenty vygenerovány',
                    error: 'Chyba při generování dokumentů',
                }, {
                    autoClose: 2000,
                });
            } else {
                // create intership row and redirect to detail
                axios.post(`./internship`, {
                    ...submittedData,
                    student_id: userData.userName,
                    student_num: userData.osCislo,
                    created_by: userData.userName,
                    course: course.id,
                    template: 1
                }).then(response => {
                    a(() => {
                        setShowSuccessAlert(true);
                        setTimeout(() => history.push(`/detail-praxe/${response.data.id}`), 3000);
                    });

                });
            }
        }
    }, [userData, startDate, history, endDate, internshipToRegenerate, course, businessDays, confirmDialog, contracts]);


    const loadOptions = async (searchedValue) => {
        const response = await nonAuthAxioms.get(`${API_BASE_URL}/ares/search/${searchedValue}`);
        let data = [];
        if (response?.data) {
            data = response.data.map(company => ({...company, value: company.ico}))
        }

        return data;
    };

    const CompanyOption = (props) => {
        const filterValue = input;
        const option = props.data || false;

        const regexp = new RegExp('(' + filterValue + ')', 'gi');
        let ico = filterValue && option.ico ? String(option.ico).replace(regexp, '<mark>$1</mark>') : option.ico;
        let name = filterValue && option.name ? option.name.replace(regexp, '<mark>$1</mark>') : option.name;

        return <components.Option {...props} className='option'>
            <div className="company-option">
                <div className="ico" dangerouslySetInnerHTML={{__html: ico}}/>
                <div className="name" dangerouslySetInnerHTML={{__html: name}}/>
            </div>
        </components.Option>;
    };

    return <div className={(isLoading ? ' commonLoadingSpinner ' : '') + "main-container"} style={{width: 750}}>
        {!!props.backButton ? props.backButton : null}
        <SimplePopUpAlert show={showSuccessAlert} message="Praxe byla úspěšně vytvořena."/>
        <Dialog ref={confirmDialog}/>

        {!!internshipToRegenerate && <Alert variant="secondary d-flex justify-content-center">
            Režim pro znovu vygenerování smlouvy, prosím vyplňte chybějící pole.
        </Alert>}
        <Form.Group>
            <Row className="d-flex justify-content-center">
                <h3 style={{width: 'auto'}}>{(course.department && course.department + '/').toUpperCase()}{course.name.toUpperCase()}</h3>
            </Row>
            <Row className="serach-company-row">
                <Col xs="10">
                    <Form.Label>
                        Vyhledat společnost
                    </Form.Label>
                    {/*<Form.Control ref={input_ico_ref} placeholder="Zadejte IČO společnosti"
                                   {...inputIcoEvents}/>*/}
                    <AsyncSelect
                        classNamePrefix="react-select"
                        ref={input_ico_ref}
                        placeholder="Zadejte IČO společnosti nebo její název"
                        isSearchable
                        cacheOptions
                        defaultValue={internshipToRegenerate ? {label: internshipToRegenerate.ico} : null}
                        isDisabled={!!internshipToRegenerate}
                        loadOptions={loadOptions}
                        inputValue={input}
                        removeSelected={false}
                        components={{Option: CompanyOption}}
                        noOptionsMessage={() => 'Žádná možnost (po zadání IČO stiskněte lupu)'}
                        onInputChange={(value, action) => {
                            // Rewrite serach value only on typing (prevent from removing on close-menu & etc. selects events)
                            if (action.action === "input-change") setInput(value);
                        }}
                        onChange={({ico}) => {
                            setInput("");
                            icoEnteredHandle(ico);
                        }}
                    />
                </Col>
                <Col>
                    <Button onClick={() => icoEnteredHandle(input)} disabled={!!internshipToRegenerate}>
                        <Icon icon="search"/>
                    </Button>
                </Col>
            </Row>
        </Form.Group>
        {error && <Alert variant="danger">{error}</Alert>}
        {!error && companyData && <>
            <Form onSubmit={handleSubmit} key={companyData?.ICO || ''} noValidate validated={validated}>
                <Row>
                    <Col className="form-part">
                        <HeadLineSeparator text="Společnost poskytující praxi" style={{marginBottom: 0}} required/>
                        <AutoRow>
                            <BInputLabeled name="spolecnost_jmeno" label="Jméno společnosti"
                                           defaultValue={companyData.OF} required/>
                        </AutoRow>
                        <AutoRow>
                            <BInputLabeled label="IČO" name="spolecnost_ico" defaultValue={companyData.ICO} required/>
                            <BInputLabeled label="DIČ" name="spolecnost_dic" defaultValue={companyData.DIC}/>
                        </AutoRow>
                        <AutoRow colsProps={{0: {xs: 7}}}>
                            <BInputLabeled label="Sídlo" name="spolecnost_sidlo"
                                           defaultValue={createProperAdress(companyData)} required/>
                            <BInputLabeled label="Zastoupení" name="spolecnost_zastopeni" required/>
                        </AutoRow>
                    </Col>
                    <Col className="form-part">
                        <HeadLineSeparator text="Mé údaje" style={{marginBottom: 0}}/>
                        <AutoRow>
                            <BInputLabeled defaultValue={userData.nazevSp} label="Studijní program"
                                           name="student_studijni_program" required/>
                        </AutoRow>
                        <AutoRow>
                            <BInputLabeled label="Jméno" defaultValue={userData.jmeno} name="student_jmeno" required/>
                            <BInputLabeled label="Příjmení"
                                           name="student_prijmeni"
                                           required
                                           defaultValue={userData.prijmeni ? userData.prijmeni.charAt(0) + userData.prijmeni.slice(1).toLowerCase() : userData.prijmeni}/>
                        </AutoRow>
                        <AutoRow colsProps={{0: {xs: 7}}}>
                            <BInputLabeled label="Trvalé bydliště" name="student_trvale_bydliste" required/>
                            <BInputLabeled label="Datum narození" name="student_datum_narozeni" type={DATE_FIELD}/>
                        </AutoRow>
                    </Col>
                    <Col className="form-part">
                        <HeadLineSeparator text="Základní informace"/>
                        <Row>
                            {course.approximate_days_for_completion &&
                                <Row className="d-flex justify-content-center">
                                    Rozsah praxe by měl být: {course.approximate_days_for_completion} Dnů
                                </Row>}
                        </Row>
                        <AutoRow>
                            <BInputLabeled label="Začátek praxe" name="praxe_zacatek" type={DATE_FIELD}
                                           selected={startDate}
                                           onChange={(date) => setStartDate(date)}
                                           selectsStart
                                           startDate={startDate}
                                           endDate={endDate}
                                           required
                            />
                            <BInputLabeled label="Konec praxe" name="praxe_konec" type={DATE_FIELD}
                                           selected={endDate}
                                           onChange={(date) => setEndDate(date)}
                                           selectsEnd
                                           startDate={startDate}
                                           endDate={endDate}
                                           minDate={startDate}
                                           required/>
                        </AutoRow>
                        <AutoRow style={{textAlign: 'center'}}>
                            {startDate && endDate && <>
                                ({businessDays} Pracovních dní)
                                {publicHolidays?.length > 0 && <> <br/> <span className="warning-text" onClick={() => {
                                    setShow(!show)
                                }} ref={target}>
                                     Varování: Počet státních svátků mezi daty: {publicHolidays.length} <Icon
                                    icon="info-circle" style={{cursor: 'pointer'}}/>
                                 </span></>}
                            </>}
                        </AutoRow>
                        {publicHolidays?.length > 0 && <SimpleOverlay show={show} setShow={setShow}
                                                                      style={{maxWidth: 550}}
                                                                      target={target.current}
                                                                      header="Státní svátky v intervalu"
                        >
                            <table style={{fontFamily: 'monospace, monospace', letterSpacing: -1.2}}>
                                <tbody>
                                {publicHolidays.sort((a, b) => new Date(a.date) - new Date(b.date)).map(day => <tr
                                    key={day.date}>
                                    <td><strong>{formatDate(day.date)}</strong></td>
                                    <td>- {day.holidayName}</td>
                                </tr>)}
                                </tbody>
                            </table>
                        </SimpleOverlay>}
                        <AutoRow>
                            <BInputLabeled label="Pověřená osoba" name="spolecnost_povereni"
                                           defaultValue={!!internshipToRegenerate ? internshipToRegenerate.supervisor : null}
                                           required/>
                            <BInputLabeled type={PHONE_FIELD} label="Telefoní číslo" name="spolecnost_povereni_tel"
                                           required/>
                            <BInputLabeled type={EMAIL_FIELD} label="Email" name="spolecnost_povereni_email"
                                           placeholder="email@priklad.cz" required/>
                        </AutoRow>
                        <AutoRow>
                            <BInputLabeled label="Místo výkonu" name="praxe_misto_vykonu"
                                           defaultValue={companyData.AA.NMC} required/>
                        </AutoRow>
                    </Col>
                </Row>
                <Row className="action-container">
                    <Col>
                        <Button variant="primary" type="submit">
                            {!!internshipToRegenerate ? 'Znovu vygenerovat dokumenty' : 'Vygenerovat dokumenty a vytvořit praxi'}
                        </Button>
                    </Col>
                </Row>
            </Form>
        </>}
    </div>
}


CompanyData.propTypes = {
    course: T.object.isRequired,
    backButton: T.element
}

CompanyData.defaultProps = {
    backButton: null
}
export default CompanyData;