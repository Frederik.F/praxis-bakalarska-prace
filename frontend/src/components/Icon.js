import React from 'react';
import T from 'prop-types';

export const REGURAL = 'regular';
export const SOLID = 'solid';
export const ICONFAB = 'fab';

export const LOADING_ICON = 'spinner fa-spin';

export const Icon = ({
                         weight = null, icon = '', className = '',  ariaHidden = true,
                         title = '',  width = '', style = {}, spinning = false, onClick = null
                     }) => {


    if (typeof icon === "function") {
        return icon({width, className: `svgIcon${className ? ` ${className}` : ''}`, title});
    }

    return <i onClick={onClick} className={`fa fa-${icon} ${className}${spinning ? ' fa-spin' : ''}`} title={title} aria-hidden={ariaHidden} style={style}/>;
};

Icon.propTypes = {
    className: T.string,
    icon: T.oneOfType([T.string, T.array, T.func]),
    stackClass: T.string,
    weight: T.oneOf([SOLID, REGURAL, ICONFAB, ''])
};

Icon.defaultProps = {
    className: '',
    icon: '',
    stackClass: '',
    weight: SOLID
};

export default Icon;