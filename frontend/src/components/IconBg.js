import React from "react";
import Icon from "./Icon";
import T from "prop-types";

export const IconBg = props => {

    return <div {...props} className={`icon-bg ${props.className ? ' ' + props.className : null}`}>
        <Icon icon={props.icon} title={props.title}/>
    </div>;
}


IconBg.propTypes = {
    icon: T.string.isRequired,
    className: T.string
};

IconBg.defaultProps = {
    className: null
}

export default IconBg;

