import { Link } from "react-router-dom";
import {Col, Container, Row} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Icon from "./Icon";
import {useContext} from "react";
import AuthContext from "../store/auth-context";
import {useLocation} from "react-router-dom";

const Navbar = () => {
    const authCtx = useContext(AuthContext);
    const route = useLocation();
    const routesArray = route.pathname === '/' ? [''] :route.pathname.split('/');
    const singOut = () => {
        authCtx.logout();
    };

    if (!authCtx.isLoggedIn) {
        return null;
    }

    if(authCtx.isLoading){
        return null;
    }

    return <nav className="main-nav">
        <Container>
            <Row>
                <Col className="nav-tree">
                {routesArray.map(name => {
                    let to = name;
                    if (!name) name = 'Praxis';
                    return <Link key={name} to={`/${to}`}>{name.replaceAll('-',' ')}</Link>;
                }).reduce((prev, curr) => {
                    return [prev, ' / ', curr];
                })}
                </Col>
                <Col className="right-actions">
                    {authCtx.isLoggedIn &&
                        <>
                            <span className="user-name">
                                  {authCtx.userData.userName}
                            </span>

                            <Button onClick={singOut}>
                                <span style={{marginRight: 10}}>Odhlásit se</span>
                                <Icon icon="sign-out-alt"/>
                            </Button>
                        </>
                    }
                </Col>
            </Row>
        </Container>
    </nav>;
}

export default Navbar;