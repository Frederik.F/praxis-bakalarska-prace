import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import {Alert, Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import HeadLineSeparator from "../Head-line-separator";
import Select from "react-select";
import AuthContext from "../../store/auth-context";
import {useHistory} from "react-router-dom";
import {SCHOOLS_LIST} from "../../config";

const AuthForm = () => {
    const userIdRef = useRef();
    const passwordRef = useRef();
    const loginButtonRef = useRef();
    const history = useHistory();
    const authCtx = useContext(AuthContext);

    const [error, setError] = useState('');
    let schools = SCHOOLS_LIST;

    // remove DEMO in production version
    if (!(!process.env.NODE_ENV || process.env.NODE_ENV === 'development')) {
        schools = schools.filter(s => s.isDev !== true);
    }


    const handleDefaultselectedSchoolURL = () => {
        const lastUsedSchool = localStorage.getItem('selectedSchoolURL');
        const foundSchool = schools.find(school => school.value === lastUsedSchool);
        if (!foundSchool) localStorage.removeItem('selectedSchoolURL');

        const defSchool = foundSchool || schools[0];
        localStorage.setItem('selectedSchoolURL', defSchool.value);
        return defSchool;
    };

    const [usedURL, _setUsedURL] = useState(handleDefaultselectedSchoolURL);
    const setUsedURL = useCallback(selectedObj => {
        localStorage.setItem('selectedSchoolURL', selectedObj.value);
        _setUsedURL(selectedObj);
    }, []);



    useEffect(() => {
        const url = new URL(window.location.href);
        const stagUserTicket = url.searchParams.get("stagUserTicket");

        if (!authCtx.isLoggedIn && stagUserTicket) {
            const stagUserInfo = url.searchParams.get("stagUserInfo")
            const userData =  stagUserInfo ? atob(stagUserInfo) : null;
            // expiration time of token is 2 hour
            const expirationTime = new Date(new Date().getTime() + 60 * 60 * 1000 * 2);
            authCtx.login(stagUserTicket, expirationTime.toISOString(),  userData);
            history.replace('/');
        }
    }, [authCtx, history])


    const handleLogin = useCallback(() => {
        if (!usedURL) {
            setError('Škola není vybrána!');
        } else {
            window.location.href = `${usedURL.value}ws/login?originalURL=${encodeURIComponent(`${window.location.protocol + '//' + window.location.host + window.location.pathname}/Prihlaseni`)}`;
        }
    }, [usedURL]);

    return <div id="login-form">
        <HeadLineSeparator text="Přihlášení do Praxis"/>
        <div className="redirect-to-stag">
            <a className="form-text" href="http://stag.ujep.cz/index.php/pristup">
                Přesměrovat na IS STAG
            </a>
        </div>
        {schools.length > 1 && <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Škola</Form.Label>
            <Select
                classNamePrefix="react-select"
                options={schools}
                defaultValue={usedURL}
                onChange={selected => {
                    setUsedURL(selected);
                    loginButtonRef.current.focus();
                    if (selected.value === 'https://stag-ws.zcu.cz/') {
                        //setColorVariant('light','plzen')
                    }else{
                        //setColorVariant('light','default')
                    }
                }
                }
                isSearchable
                required
            />
        </Form.Group>
        }
        {false && <>
            <HeadLineSeparator>
                <h6>Přihlašovací údaje</h6>
            </HeadLineSeparator>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Přihlašovací jméno</Form.Label>
                <Form.Control type="text" placeholder="Přihlašovací jméno" ref={userIdRef} required/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Heslo</Form.Label>
                <Form.Control type="password" placeholder="Heslo" ref={passwordRef} required/>
            </Form.Group>
        </>}
        {error && <Alert variant="danger">
            {error}
        </Alert>}

        <Button variant="primary" type="submit" button-type="button" onClick={handleLogin} ref={loginButtonRef}>
            Přihlásit
        </Button>

        {false && <Form.Text className="switch-to-english" onClick={() => {
            alert('Zatím není k dizpozici')
        }}>
            Switch to English
        </Form.Text> }
    </div>;
};

export default AuthForm;
