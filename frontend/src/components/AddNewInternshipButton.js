import Icon from "./Icon";
import Button from "react-bootstrap/Button";
import React, {useContext} from "react";
import {useHistory} from "react-router-dom";
import AuthContext from "../store/auth-context";

export const AddNewInternshipButton = props => {
    const history = useHistory();
    const authCtx = useContext(AuthContext);

    return authCtx.isAdmin ?
        null
        :
        <Button className="btn-sm" onClick={props.onClick ? props.onClick :() => {
        history.push(`/`, {addNew: true})
    }}>
        <Icon icon="plus"/>
        &nbsp;
        Vytvořit novou praxi
    </Button>;
}

export default AddNewInternshipButton;