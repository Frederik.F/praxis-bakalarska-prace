import React, {forwardRef} from 'react';
import {Form} from "react-bootstrap";
import {DATE_FIELD, TEXT_FIELD} from "./Table";
import CzDatePicker from "./CzDatePicker";


const BInputLabeled = forwardRef((props, ref) => {
    const renderSwitch = (fieldType) => {
        switch (fieldType) {
            case DATE_FIELD:
                return <CzDatePicker placeholder={props.placeholder || props.label}
                                     {...props}/>;
            default:
                return <>
                    <Form.Control ref={ref} placeholder={props.label} {...props} style={{opacity: 1}}/>
                </>;
        }
    };

    const fieldType = props.type || TEXT_FIELD;

    return <Form.Group>
        <Form.Label>{props.label}</Form.Label>
        {renderSwitch(fieldType)}
    </Form.Group>
});

export default BInputLabeled;