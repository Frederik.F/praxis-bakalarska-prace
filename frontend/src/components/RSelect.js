import React, {useRef, useState} from "react";
import {Form} from "react-bootstrap";
import Select from "react-select";


export const RSelect = props => {
    const [value, setValue] = useState(props.defaultValue?.value);


    return <><Select {...props} classNamePrefix="react-select" styles={{
        control: (provided, state) => ({
            ...provided,
            minHeight: props.height,
            height: props.height,
            boxShadow: state.isFocused ? null : null,
        }),

        valueContainer: (provided, state) => ({
            ...provided,
            height: props.height,
            padding: '0 6px',
            fontSize: props.fontSize,
            fontWeight: 400
        }),

        input: (provided, state) => ({
            ...provided,
            margin: '0px',
        }),
        indicatorSeparator: state => ({
            display: 'none',
        }),
        indicatorsContainer: (provided, state) => ({
            ...provided,
            height: props.height,
        }),
    }}
     onChange={(selected) => {
        if(typeof props.onChange === 'function') props.onChange(selected)
        setValue(selected?.value);

     }}
    />
        {!props.disabled && (
            <Form.Control
                name={props.name}
                tabIndex={-1}
                autoComplete="off"
                style={{opacity: 0, height: 0}}
                value={value}
                required={props.required}
                onChange={() => {}}
            />
        )}
    </>
};

export default RSelect;
