import React, {forwardRef, useContext, useImperativeHandle, useRef, useState} from "react";
import T from "prop-types";
import axios from "../hooks/auth-axios";
import AuthContext from "../store/auth-context";
import {generateUniqId} from "../utils/Utils";
import {toast} from "react-toastify";

export const TemplateUploader = forwardRef((props, ref) => {
    const authCtx = useContext(AuthContext);
    const userData = authCtx.userData;
    const fileInputRef = useRef();

    const [key, setKey] = useState(generateUniqId());
    const [type, setType] = useState(null);
    const [name, setName] = useState(null);

    const addNewTemplateHandler = async () => {
        const docs = fileInputRef.current;
        //const reader = new FileReader();
        if(!type){
            console.error('type is required!');
            toast.error('Při nahrávání nastala chyba', {
                position: "top-center",
                autoClose: 2000,
            });

            return false;
        }

        if (docs.files.length === 0) {
            return false;
        }

        const formData = new FormData();
        formData.append("templateFile", docs.files[0]);
        formData.append('created_by', userData.userName);
        formData.append('type', type);
        formData.append('name', name);

        const createdTemplate = await axios.post(`./template`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        if(createdTemplate.data.error_cs){
            toast.error(createdTemplate.data.error_cs, {
                position: "top-center",
                autoClose: 2000,
            });
            setName(null);
            setType(null);

            setKey(generateUniqId());
            return true;
        }

        if(props.successMessage){
            toast.success(props.successMessage, {
                position: "top-right",
                autoClose: 2000,
            });
        }

        if(typeof props.onAfterUpload === "function"){
            props.onAfterUpload(createdTemplate.data);
        }
    };

    useImperativeHandle(ref, () => ({
        click(type, name) {
            setType(type);
            setName(name);
            fileInputRef.current.click()
        }
    }));

    return <React.Fragment key={key}>
        <input
            type="file"
            accept=".docx, application/msword"
            ref={fileInputRef}
            style={{display: 'none'}}
            onChange={addNewTemplateHandler}
        />
    </React.Fragment>
});

TemplateUploader.propTypes = {
    onAfterUpload: T.func,
    successMessage: T.string,
    variant: T.string

};

TemplateUploader.defaultProps = {
    onAfterUpload: null,
    successMessage: '',
    variant: 'primary'
};


export default TemplateUploader;