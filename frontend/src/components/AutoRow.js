import {Col, Row} from "react-bootstrap";
import React from "react";

export const AutoRow = ({children, colsProps, style}) => {
    let i = -1;
    return <Row style={style}>
        {React.Children.map(children, child => {
            i++;
            const colProps = colsProps && colsProps.hasOwnProperty(i) ? colsProps[i] : {};
            return <Col {...colProps}>
                {React.cloneElement(child, {style: {...child.props.style}})}
            </Col>

        })}
    </Row>
};

export default AutoRow;