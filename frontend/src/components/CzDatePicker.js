import React, {useState} from 'react';
import DatePicker, {registerLocale} from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import cs from "date-fns/locale/cs";
import {dateFormat} from "../config"; // the locale you want
registerLocale("cs", cs); // register it with the name you want


const CzDatePicker = props => {
    const [selected, setSelected] = useState(null);

    return <DatePicker placeholderText={dateFormat.toLowerCase()}
                       locale="cs"
                       isClearable={false}
                       dateFormat='dd.MM.yyyy'
                       adjustDateOnChange
                       autoComplete="off"
                       className="cz-datepicker form-control"
                       selected={selected}
                       onChange={date => setSelected(date)}
                       {...props}/>
}

export default CzDatePicker;