import React from 'react';
import Modal from "react-modal";

export const DocViewerModal = props => <Modal style={{content: {overflow: 'visible', ...(props.style?.content || {})}}} {...props}>
    {props.children}
</Modal>

export default DocViewerModal;