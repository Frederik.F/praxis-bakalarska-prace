import React from 'react'
import T from "prop-types";
import DocViewerModal from "./DocViewerModal";
import {Modal} from "react-bootstrap";

export const GOOGLE = 'GOOGLE';
export const OFFICE = 'OFFICE';

export const DocumentViewer = props => {
    if(props.inModal){
        let defStyles = {
            borderBottomLeftRadius: 'var(--border-radius-large)',
            borderBotomRightRadius: 'var(--border-radius-large)',
        };

        if(props.fileModalHeader) defStyles = {borderRadius: 'var(--border-radius-large)'};
        return <DocViewerModal
            isOpen
            shouldCloseOnOverlayClick
            style={{
                content: {
                    padding: 0,
                    width: 900,
                    margin: 'auto',
                    overflow: 'visible'
                }
            }}
            //prefixClass="document-viewver-modal"
            className="document-viewver-modal"
            ariaHideApp={false}
            {...props.modalProps}
        >

            {props.fileModalHeader && <Modal.Header className="d-flex justify-content-center">
                {props.fileModalHeader}
            </Modal.Header>}
            <Viewer style={{...defStyles, ...props.style}} {...props}/>
        </DocViewerModal>
    }

    return <Viewer  {...props}/>
};

export const Viewer = props => {
    const {url, provider, width, height, style, key} = props;
    if (provider === OFFICE) {
        return <iframe title="file-iframe-viewer" key={key}
            src={`https://view.officeapps.live.com/op/embed.aspx?src=${url}`}
            style={style}
            width={width} height={height}
            frameBorder='0'>This is an embedded
            <a rel="noreferrer" target='_blank' href='http://office.com'>Microsoft Office</a>
            document, powered by <a rel="noreferrer" target='_blank' href='http://office.com/webapps'>Office Online</a>.
        </iframe>
    }

    return <iframe title="template view" className={'.docx'} width={width} height={height} frameBorder="0" style={style}
                   src={`https://docs.google.com/gview?url=${url}&embedded=true`}/>

};

DocumentViewer.propTypes = {
    url: T.string.isRequired,
    provider: T.string,
    width: T.string,
    height: T.string,
    style: T.object,
    inModal: T.bool,
    modalProps: T.object,
    fileModalHeader: T.string,
};

DocumentViewer.defaultProps = {
    provider: OFFICE,
    width: "100%",
    height: "1027px",
    inModal: false,
    modalProps: {},
    style: {},
    fileModalHeader: '',
};


export default DocumentViewer;