export const HeadLineSeparator = ({children, text, className, style}) => {

    return <div className={`head-line-separator${className ? ' ' + className : ''}`} style={style}>
        {children ? children : <h4><span>{text}</span></h4>}
    </div>;
}

export default HeadLineSeparator;