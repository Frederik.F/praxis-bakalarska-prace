export const LoadingDots = props => {
    return <div className="loading-dots" {...props}>...</div>
}

export default LoadingDots;