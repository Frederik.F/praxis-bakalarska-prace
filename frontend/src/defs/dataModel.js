import Icon from "../components/Icon";
import React from "react";
import moment from "moment";
import axios from "../hooks/auth-axios";
import {Comparator} from "react-bootstrap-table2-filter";

export const modelInternship = {
    state: {
        options: {
            a: 'Čeká na schválení',
            z: 'Zadána', // state that are resolved on server side
            p: 'Probíhající', // state that are resolved on server side
            d: 'Dokončeno', // state that are resolved on server side
            c: 'Splněno',
        },
        render: (value, startDate, endDate, textOnly = false) => {
            if (value === 'a') {
                if (textOnly) return 'Čeká na schválení';
                return <><Icon icon="spinner" spinning/> &nbsp; <span>Čeká na schválení </span></>
            }
            if (value === 'i') {
                //Is before
                if (moment().isBefore(moment(startDate))) {
                    if (textOnly) return 'Zadána';
                    return <><Icon icon="file"/> &nbsp; <span>Zadána</span></>;
                }

                if (moment().isBetween(moment(startDate), moment(endDate), 'days', '[]')) {
                    if (textOnly) return 'Probíhající';
                    return <><Icon icon="play"/> &nbsp; <span>Probíhající</span></>;
                }
                if (textOnly) return 'Dokončeno';
                return <><Icon icon="flag-checkered"/> &nbsp; <span>Dokončeno</span></>
            }
            if (value === 'c') {
                if (textOnly) return 'Splněno';
                return <span style={{color: 'green'}}><Icon icon="check"/> &nbsp; <span>Splněno</span></span>;
            }
        }
    }
}

export const modelTemplate = {
    type:{
        options: {
            'c': 'Smlouva',
            'f': 'Cílový dokument'
        }
    }
}

export const getCourseOptionsFromDb = async () => axios.get(`./course`,{
    params:{
        filter: [{
            fieldName: 'active',
            filterVal: 'a',
            comparator: Comparator.EQ,
        }]
    }
}).then(response => {
    if (!!response.data && response.data.length > 0) {
        const a = {};
        response.data.forEach(c => {
            a[c.id] = `${c.department}/${c.name}`;
        })
        return a;
    }

    return {};
});
