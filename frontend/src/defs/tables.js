import React from 'react';
import {CUSTOM_FIELD, DATE_FIELD, DATE_TIME_FIELD, NUMBER_FIELD, TEXT_FIELD} from "../components/Table";
import {Comparator, dateFilter, selectFilter, textFilter} from "react-bootstrap-table2-filter";
import {modelInternship, modelTemplate} from "./dataModel";


export const ADMIN_TEMPLATE_TABLE = ([{
    fieldType: TEXT_FIELD,
    dataField: 'name',
    text: 'Název',
    sort: true,
    filter: textFilter({
        comparator: Comparator.LIKE,
        placeholder: 'Název'
    }),
},{
    fieldType: CUSTOM_FIELD,
    dataField: 'type',
    text: 'Typ',
    sort: true,
    filter: selectFilter({
        options: modelTemplate.type.options,
        placeholder: 'Vyberte stav',
    }),
    formatter: (cell, row) => modelTemplate.type.options[row.type]
}, {
    fieldType: DATE_TIME_FIELD,
    dataField: 'updated_at',
    text: 'Poslední změna',
    sort: true,

}, {
    fieldType: DATE_TIME_FIELD,
    dataField: 'created_at',
    text: 'Vytvořeno',
    sort: true,
},
]);

export const ADMIN_COURSE_TABLE = [{
    fieldType: NUMBER_FIELD,
    dataField: 'id',
    text: 'id',
    sort: true,
},{
    fieldType: TEXT_FIELD,
    dataField: 'department',
    text: 'Katedra',
    sort: true,
    filter: textFilter({
        comparator: Comparator.EQ,
        placeholder: 'Přesný název katedry'
    }),
    headerStyle: (colum, colIndex) => ({ width: 110})
},{
    fieldType: TEXT_FIELD,
    dataField: 'name',
    text: 'Název',
    sort: true,
    filter: textFilter({
        comparator: Comparator.EQ,
        placeholder: 'Přesný název katedry'
    }),
},
];

export const STUDENT_INTERSHIP_TABLE = ((courses) => [{
    fieldType: DATE_TIME_FIELD,
    dataField: 'created_at',
    text: 'Vytvořena',
    sort: true,
}, {
    fieldType: TEXT_FIELD,
    dataType: NUMBER_FIELD,
    dataField: 'course',
    filterReplaceName: 'course_id',
    text: 'Kurz',
    sort: false,
    filter: selectFilter({
        options: courses,
        placeholder: 'Vyberte kurz',
    }),
    related: {
        select: {
            name: true,
            department: true
        }
    },
    formatter: (cell) => `${cell.department}/${cell.name}`
}, {
    fieldType: CUSTOM_FIELD,
    dataField: 'state',
    text: 'Stav praxe',
    sort: false,
    filter: selectFilter({
        options: modelInternship.state.options,
        placeholder: 'Vyberte stav',
    }),
    formatter: (cell, row) => modelInternship.state.render(cell, row.start_date, row.end_date)
}, {
    fieldType: DATE_FIELD,
    dataField: 'start_date',
    text: 'Začátek praxe',
    sort: true,
    filter: dateFilter({
        withoutEmptyComparatorOption: true,
        comparator: Comparator.GE,
        comparators: [Comparator.GE],
        comparatorClassName: "simple-text",
        dateClassName: 'wide'
    })
}, {
    fieldType: DATE_FIELD,
    dataField: 'end_date',
    text: 'Konec praxe',
    sort: true,
    filter: dateFilter({
        withoutEmptyComparatorOption: true,
        comparator: Comparator.LE,
        comparators: [Comparator.LE],
        comparatorClassName: "simple-text",
        dateClassName: 'wide'
    })
},
]);

export const ADMIN_HOME_TABLE = ((courses) => [{
    fieldType: DATE_TIME_FIELD,
    dataField: 'created_at',
    text: 'Vytvořena',
    sort: true,
}, {
    fieldType: TEXT_FIELD,
    dataField: 'student_num',
    text: 'Student',
    sort: true,
    filter: textFilter({
        comparator: Comparator.LIKE,
        placeholder: 'FXXXXX'
    }),
}, {
    fieldType: TEXT_FIELD,
    dataType: NUMBER_FIELD,
    dataField: 'course',
    filterReplaceName: 'course_id',
    text: 'Kurz',
    sort: false,
    filter: selectFilter({
        options: courses,
        placeholder: 'Vyberte kurz',
    }),
    related: {
        select: {
            name: true,
            department: true
        }
    },
    formatter: (cell) => `${cell.department}/${cell.name}`
}, {
    fieldType: CUSTOM_FIELD,
    dataField: 'state',
    text: 'Stav praxe',
    sort: false,
    filter: selectFilter({
        options: modelInternship.state.options,
        placeholder: 'Vyberte stav',
    }),
    formatter: (cell, row) => modelInternship.state.render(cell, row.start_date, row.end_date)
}, {
    fieldType: DATE_FIELD,
    dataField: 'start_date',
    text: 'Začátek praxe',
    sort: true,
    filter: dateFilter({
        withoutEmptyComparatorOption: true,
        comparator: Comparator.GE,
        comparators: [Comparator.GE],
        comparatorClassName: "simple-text",
        dateClassName: 'wide'
    })
}, {
    fieldType: DATE_FIELD,
    dataField: 'end_date',
    text: 'Konec praxe',
    sort: true,
    filter: dateFilter({
        withoutEmptyComparatorOption: true,
        comparator: Comparator.LE,
        comparators: [Comparator.LE],
        comparatorClassName: "simple-text",
        dateClassName: 'wide'
    })
},
]);