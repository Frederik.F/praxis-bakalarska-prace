import React, {useEffect, useState, useContext, useRef, useCallback, useMemo} from 'react';
import {useHistory, useParams} from "react-router-dom";
import axios from "../hooks/auth-axios";
import HeadLineSeparator from "../components/Head-line-separator";
import Button from "react-bootstrap/Button";
import {Col, Row} from "react-bootstrap";
import AutoRow from "../components/AutoRow";
import {
    formatDate,
    generateDocumentFromTemplate,
    getBusinessDays,
    getPublicHolidaysInterval, todayPercentageValueBetweenDates
} from "../utils/Utils";
import Icon from "../components/Icon";
import AuthContext from "../store/auth-context";
import SimplePopUpAlert from "../components/SimplePopupAlert";
import SimpleOverlay from "../components/SimpleOverlay";
import {modelInternship} from "../defs/dataModel";
import StateDiagram from "../components/State-diagram";
import moment from "moment";
import AddNewInternshipButton from "../components/AddNewInternshipButton";

const IntershipDetail = props => {
    const authCtx = useContext(AuthContext);
    const userData = authCtx.userData;
    const history = useHistory();
    const {id} = useParams();

    const [internship, setInternShip] = useState({});
    const [companyDetails, setCompanyDetails] = useState({});
    const [course, setCourse] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [show, setShow] = useState(false);
    const [simpleAlertMessage, setSimpleAlertMessage] = useState(null);

    const [publicHolidays, setPublicHolidays] = useState([]);
    const [showPublicHolidays, setShowPublicHolidays] = useState(false);
    const publicHolidaysTarget = useRef();

    const daysTillEnd = internship?.end_date ? moment(internship.end_date).diff(moment(new Date()), 'days') : null;

    const contracts = useMemo(() => (internship?.Template || []).filter(t => t.type === 'c'),[internship?.Template]);

    const finalDocuments = useMemo(() => (internship?.Template || []).filter(t => t.type === 'f'),[internship?.Template]);

    useEffect(() => {
        let preventMemeoryLeak = true;

        async function fetchMyAPI() {
            setIsLoading(true);
            const internship = await axios.get(`./internship/find/withTemplatesAndCourse/${id}`);

            if(!internship.data){
                setInternShip(null);
                return true;
            }

            const course = internship.data.course_id ? await axios.get(`./course/find/${internship.data.course_id}`) : {};
            const company = await axios.get(`./ares/${internship.data.ico}`);
            const holidays = await getPublicHolidaysInterval(internship.data.start_date, internship.data.end_date, false);

            if (preventMemeoryLeak) {
                setIsLoading(false);
                setInternShip(internship.data);
                setCompanyDetails(company.data);
                setCourse(course.data);
                setPublicHolidays(holidays);
            }
        }

        fetchMyAPI().catch(console.error);

        // cancel any future `setData`
        return () => preventMemeoryLeak = false;

    }, [id])


    const generateDocuments = useCallback(() => {
        finalDocuments.forEach(template => {
            const courseName = course.department + '-'+course.name;
            generateDocumentFromTemplate(`praxe.docx`, null, `${userData.prijmeni}_${courseName}_${template.name}`);
        });
    }, [userData.prijmeni, course, finalDocuments]);

    const handleChangeInternshipState = state => {
        setSimpleAlertMessage( state === 'i' ? 'Praxe byla úspěšně scvhálena' : 'Praxe byla úspěšně splněna');
        axios.patch(`./internship/${id}`, {state, updated_by: userData.userName}).then(response => {
            setShow(true);
            setInternShip({...internship, state});
            setTimeout(() => setShow(false), 1000);
        });
    }


    if (!internship) {
        setTimeout(() => {
            history.push('/');
        }, 3000);
        return <SimplePopUpAlert show={true} message={"Praxe nebyla nalezena! Budete přesměrováni..."}/>;
    }


    const MyLabel = ({label, value}) => <div>
        <b>{label}:</b> <span>{value}</span>
    </div>

    const getPercentageForState = state => {
        if (state === 'a') {
            return (['a', 'i', 'c'].includes(internship.state) ? 100 : 0);
        }
        if (state === 'z') {
            return (['i', 'c'].includes(internship.state) ? 100 : 0);
        }
        if (state === 'i') {
            if (['i'].includes(internship.state) && moment().isAfter(moment(internship.start_date))) {
                return todayPercentageValueBetweenDates(internship.start_date, internship.end_date);
            } else {
                return internship.state === 'c' ? 100 : 0;
            }
        }
        if (state === 'd') {
            if (internship.state === 'c') return 100;
            return (['i', 'c'].includes(internship.state) && moment().isAfter(moment(internship.end_date).add(1, 'day')) ? 100 : 0);
        }
        if (state === 'c') {
            return internship.state === 'c' ? 100 : 0;
        }
    }

    const fullCourseName = !!course ? course.department && course.department + '/'+course.name : '';
    const businessDays = !!internship ? getBusinessDays(internship.start_date, internship.end_date) : 0;

    return <div className={(isLoading ? ' commonLoadingSpinner blind' : '')}>
        <SimplePopUpAlert show={show} message={simpleAlertMessage}/>
        {publicHolidays.length > 0 && <SimpleOverlay show={showPublicHolidays} setShow={setShowPublicHolidays}
                                                     style={{maxWidth: 550}}
                                                     target={publicHolidaysTarget.current}
                                                     header="Státní svátky v intervalu"
        >
            <table style={{fontFamily: 'monospace, monospace', letterSpacing: -1.2}}>
                <tbody>
                {publicHolidays.sort((a, b) => new Date(a.date) - new Date(b.date)).map(day => <tr key={day.date}>
                    <td><strong>{formatDate(day.date)}</strong></td>
                    <td>- {day.holidayName}</td>
                </tr>)}
                </tbody>
            </table>
        </SimpleOverlay>}
        <div className="main-container" style={{width: 1000}}>
            <Row>
                <Col className="d-flex justify-content-end">
                    <AddNewInternshipButton />
                </Col>
            </Row>
            <HeadLineSeparator text={`Stav praxe${fullCourseName ? ' '+fullCourseName.toUpperCase(): ''}`} style={{marginBottom: 0}}/>
            {authCtx.isAdmin  && internship.state === 'a' && internship.course?.id && internship.course.approximate_days_for_completion > businessDays && <Col className="text-center" style={{fontWeight: 500}}>
                    <span className="warning-text">
                        Upozornení: Praxe má pouze {businessDays} pracovních dní místo {internship.course.approximate_days_for_completion} dní pro řádné splnění.
                    </span>
            </Col>}
            <Row>
                <Col>
                    <b>Stav:</b>&nbsp;
                    <span>{modelInternship.state.render(internship.state, internship.start_date, internship.end_date, true)}</span>
                    {authCtx.isAdmin && internship.state === 'a' &&
                        <>
                            <Button style={{marginLeft: 10}} className="btn-sm" onClick={() => handleChangeInternshipState('i')}>
                                Schválit praxi
                            </Button>
                        </>
                    }
                    {authCtx.isAdmin && !['a','c'].includes(internship.state) && internship.state &&
                    <Button style={{marginLeft: 10}} className="btn-sm" onClick={() => handleChangeInternshipState('c')}>
                        Splnit praxi
                    </Button>
                    }
                </Col>
            </Row>
            <Row>
                <Col xs="4">
                    <MyLabel label={'IČO'} value={internship.ico}/>
                </Col>
                <Col xs="auto">
                    <div>
                        <b>Název firmy:</b> <span>{companyDetails.OF}
                        <a className="no-styles" rel="noreferrer"
                           href={`https://rejstrik-firem.kurzy.cz/hledej/?s=${companyDetails.ICO}`} target="_blank">
                            <Icon icon="search" style={{fontSize: '0.8rem', cursor: 'pointer', marginLeft: 5}}
                                  title="Zobrazit detail firmy"/> </a>
                    </span>
                    </div>
                </Col>
            </Row>
            <AutoRow>
                <MyLabel label={'Dohlížející'} value={internship.supervisor}/>
                <AutoRow>
                    <MyLabel label={'Začátek praxe'} value={formatDate(internship.start_date)}/>
                    <div>
                        <MyLabel label={'Konec praxe'} value={formatDate(internship.end_date)}/>
                        <div className="d-flex justify-content-center" ref={publicHolidaysTarget}
                             style={{fontSize: '0.9rem'}}> ({businessDays} Pracovních
                            dní)
                            {publicHolidays?.length > 0 &&
                                <span className="warning-text"
                                      onClick={() => setShowPublicHolidays(!showPublicHolidays)}>
                                    &nbsp;<Icon icon="info-circle"
                                                style={{cursor: 'pointer'}}/>
                                </span>}
                        </div>
                    </div>
                </AutoRow>
            </AutoRow>
            <Row style={{marginTop: 15}}>
                <StateDiagram states={[
                    {
                        icon: 'pause',
                        description: 'Čeká na schválení',
                        percentage: getPercentageForState('a')
                    },
                    {
                        icon: 'file',
                        description: <>Zadána
                            {!authCtx.isAdmin && contracts.length > 0 &&
                                <Button size="sm" style={{left: -20}} onClick={() => {
                                    history.push(`/`,{internship, course, contracts})
                                }}>Generovat smlouvu</Button>}
                        </>,
                        percentage: getPercentageForState('z')
                    },
                    {
                        icon: 'play',
                        description: <>Probíhající <span style={{fontSize: '0.8rem'}}>
                            {daysTillEnd > 0 && <span>(Zbývá {daysTillEnd} dní)</span>}
                        </span></>,
                        percentage: getPercentageForState('i')
                    },
                    {
                        icon: 'flag-checkered',
                        description: <>Dokončeno
                            {!authCtx.isAdmin && finalDocuments.length > 0 &&
                                <Button style={{left: -45}} size="sm" onClick={generateDocuments}>
                                    Generovat cílové dokumenty
                                </Button>}
                        </>,
                        percentage: getPercentageForState('d')
                    },
                    {
                        icon: 'check',
                        description: 'Splněno',
                        percentage: getPercentageForState('c')
                    },
                ]}/>
            </Row>
            {!!course.description && internship.state !== 'c' && <>
                <HeadLineSeparator text="Popis"/>
                <Row dangerouslySetInnerHTML={{__html: course.description}}/>
            </>}

        </div>
    </div>;
};

export default IntershipDetail;