import React, {useState, useEffect, useContext, useMemo} from 'react';
import CompanyData from "../../components/CompanyData";
import axios from "../../hooks/auth-axios";
import {Alert, Container} from "react-bootstrap";
import Table from "../../components/Table";
import {STUDENT_INTERSHIP_TABLE} from "../../defs/tables";
import {Redirect, useHistory} from "react-router-dom";
import Button from "react-bootstrap/Button";
import AuthContext from "../../store/auth-context";
import CourseSelector from "../../components/CourseSelector";
import Icon from "../../components/Icon";

const StudentHome = props => {
    const authCtx = useContext(AuthContext);
    const history = useHistory();
    const userData = authCtx.userData;

    const [internships, setInternShips] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [selectedCourse, setSelectedCourse] = useState(userData.courses?.length === 1 ? userData.courses[0] : null);
    const [showBackButton, setShowBackButton] = useState(false);

    let internshipToRegenerate = null;
    if (props.location.state?.internship) {
        internshipToRegenerate = props.location.state?.internship;
    }
    // get all internships
    useEffect(() => {
        if (internshipToRegenerate || !!props.location.state?.addNew) return true;
        setIsLoading(true);
        axios.get(`./internship`).then(({data}) => {
            if (data.length === 1) {
                history.replace(`detail-praxe/${data[0].id}`);
                return true;
            }
            setInternShips(data);
            setIsLoading(false);
        }).catch(() => {
            setIsLoading(false);
        });
    }, [history, internshipToRegenerate, props.location.state?.addNew]);

    const coursesOptions = useMemo(() => {
        const obj = {};
        (userData.courses || []).forEach(c => {
            obj[c.id] = `${c.department}/${c.name}`;
        })
        return obj;
    }, [userData.courses])

    if (isLoading) {
        return false;
    }

    if (!internships.length && !internshipToRegenerate && userData.courses && userData.courses?.length > 1 && !selectedCourse) {
        return <CourseSelector selectCourse={course => {
            setShowBackButton(true);
            setSelectedCourse(course);
        }
        }/>
    }

    if (internshipToRegenerate) {
        return <CompanyData internship={internshipToRegenerate} course={props.location.state?.course} contracts={props.location.state?.contracts}/>
    }

    return <div>
        {internships.length === 1 && <Redirect to={`detail-praxe/${internships[0].id}`}/>}
        {internships.length > 1 && <>
            <Container className="intership-table-container">
                <Table
                    keyField="id"
                    API="internship"
                    columnsDef={STUDENT_INTERSHIP_TABLE(coursesOptions)}
                    noDataLabel="Zatím nebyla vytvotena žádná praxe"
                    remote={{
                        filter: true,
                        pagination: false,
                        sort: true,
                    }}
                    defaultOrderBy={[{created_at: 'desc'}, {state: 'desc'}, {id: 'desc'}]}
                    showPaginator={false}
                    rowEvents={{
                        onDoubleClick: (e, row) => {
                            history.push(`/detail-praxe/${row.id}`);
                        }
                    }}
                    pushedFilters={ [{
                        fieldName: 'student_id',
                        filterVal: userData.userName,
                        comparator: '='
                    }]}
                />
            </Container>
        </>}
        {internships.length === 0 && selectedCourse ?
            <CompanyData course={selectedCourse}
                         backButton={showBackButton ? <Button className="circle" onClick={() => {
                             setShowBackButton(false);
                             setSelectedCourse(null);
                         }}><Icon
                             icon="arrow-left"/></Button> : null}/> : (internships.length === 0 && !userData.courses?.length ?
                <div className="company-intership-form">
                    <Alert variant="danger" className="d-flex justify-content-center">Ve STAGu nemáte zapsaný žádný
                        předmět pro praxe</Alert>
                </div> : null)}
    </div>;
};

export default StudentHome;