import React, {useCallback, useContext, useMemo, useRef, useState} from 'react';
import {Comparator} from "react-bootstrap-table2-filter";
import AuthContext from "../../store/auth-context";
import Table from "../../components/Table";
import {ADMIN_TEMPLATE_TABLE} from "../../defs/tables";
import DocumentViewer from "../../components/Document-viewer";
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {
    addSelectedRowClass, downloadFileFromTableAPI, generateUniqId,
    getTouchedField,
    handleCURD
} from "../../utils/Utils";
import HeadLineSeparator from "../../components/Head-line-separator";
import AutoRow from "../../components/AutoRow";
import BInputLabeled from "../../components/BInputLabeled";
import {modelTemplate} from "../../defs/dataModel";
import Icon from "../../components/Icon";
import {Link, useHistory, useParams} from "react-router-dom";
import RSelect from "../../components/RSelect";
import TemplateUploader from "../../components/TemplateUploader";
import Dialog from "react-bootstrap-dialog";
import SimpleOverlay from "../../components/SimpleOverlay";
import {toast} from "react-toastify";

// have to wrapp it to get into name params
/*const DocumentViewerWrapper = props => {
    const {name} = useParams();
    return <DocumentViewer fileModalHeader={name} url={`https://praxistestik.tode.cz/templates/${name}`} {...props}/>;
}*/

export const Templates = () => {
    const authCtx = useContext(AuthContext);
    const userData = authCtx.userData;
    const history = useHistory();
    const params = useParams();

    const templateUploaderRef = useRef();
    const formRef = useRef();
    const confirmDialog = useRef();
    const relationsTarget = useRef();

    const [key, setKey] = useState(generateUniqId());

    const [showRelations, setShowRelations] = useState(false);
    const [validated, setValidated] = useState(false);
    const [selectedTemplate, setSelectedTemplate] = useState(null);

    const checkValidation = useCallback((e, form) => {
        if (form.checkValidity()) {
            e.preventDefault();
            e.stopPropagation();

            const {name, type} = Object.fromEntries(new FormData(form));
            templateUploaderRef.current.click(type, name);
        }

        setValidated(true);

    }, []);

    const typeOptions = useMemo(() => {
        return Object.entries(modelTemplate.type.options).map(([key, val]) => ({
            label: val,
            value: key
        }));
    }, [modelTemplate.type.options])

    const handleSubmit = useCallback(e => {
        e.preventDefault();
        const submittedData = Object.fromEntries(new FormData(e.target));

        const touchedFields = getTouchedField(selectedTemplate, submittedData);

        if (touchedFields !== {}) {
            if (selectedTemplate?.id) {
                handleCURD('template', selectedTemplate.id, touchedFields).catch().then(r => {
                    if(r.data.error_cs){
                        return true;
                    }
                    setSelectedTemplate({...selectedTemplate, ...touchedFields});
                    if (touchedFields.hasOwnProperty('name')) {
                        history.replace('/Sablony/' + touchedFields.name);
                    }
                    toast.success('Šablona úspěšně upravena.', {
                        position: "top-right",
                        autoClose: 2000,
                    });
                    setKey(generateUniqId());
                });
            }
        }
    }, [selectedTemplate]);

    const handleRemoveTemplate = useCallback(async () => {
        const dialog = await new Promise((resolve, reject) => {
            confirmDialog.current.show({
                title: 'Upozornění',
                body: <>Opravdu si přejete smazat šablonu <b>{selectedTemplate.name}</b>?</>,
                actions: [
                    Dialog.Action('Zrušit', () => resolve(false)),
                    Dialog.Action('Smazat', () => resolve(true), 'btn-danger')
                ],
                bsSize: 'small',
                onHide: (dialog) => {
                    dialog.hide()
                    resolve(false);
                }
            });
        });

        if (dialog === true) {
            await handleCURD(`template`, selectedTemplate.id, {
                active: 'd',
                name: selectedTemplate.name + '(deleted)',
                updated_by: userData.userName
            }, true, 'delete');
            toast.success('Šablona byla úspěšně smazána', {
                position: "top-right",
                autoClose: 2000,
            });
            setSelectedTemplate(null);
            setKey(generateUniqId());
            history.replace('/Sablony');
        }

    }, [selectedTemplate]);

    const hadnleDownloadTemplate = useCallback(async () => {
        downloadFileFromTableAPI(selectedTemplate.id, selectedTemplate.name);
    },[selectedTemplate])

    const isExistingTemplate = !!(selectedTemplate?.id);

    return <div id="templates" key={key}>
        <Dialog ref={confirmDialog}/>
        {!isExistingTemplate && <TemplateUploader ref={templateUploaderRef} onAfterUpload={(createdTemplate => {
            setKey(generateUniqId());
            setValidated(false);
        })}/>}

        <Container>
            <Row>
                <Col>
                    <Button onClick={() => {
                        history.replace('/Sablony');
                        setSelectedTemplate({})
                        document.querySelectorAll('#template-table tr.selected').forEach(row => {
                            row.classList.remove("selected")
                        });
                    }}>
                        <Icon icon="plus"/> Vytvořit novou šablonu
                    </Button>
                </Col>
            </Row>
            <Row className="flex-lg-nowrap flex-md-wrap">
                <Col md={10} lg={3} style={{minWidth: 500}}>
                    <Table
                        keyField="id"
                        id="template-table"
                        API="template/includeCourses"
                        className="layout-fixed"
                        columnsDef={ADMIN_TEMPLATE_TABLE}
                        noDataLabel="Zatím nebyl vytvořena žádná šablona"
                        defaultOrderBy={[{created_at: 'desc'}]}
                        remote={{
                            filter: true,
                            pagination: true,
                            sort: true,
                        }}
                        showPaginator={true}
                        rowEvents={{
                            onClick: (e, row) => {
                                if (!(!!selectedTemplate && selectedTemplate.id === row.id)) {
                                    setSelectedTemplate(row);
                                    addSelectedRowClass(e.target);
                                    history.replace('/Sablony/' + row.name);
                                }
                            }
                        }}
                        onDidMount={(data) => {
                            if (data.hasOwnProperty('rows') && data.totalRows > 0) {
                                let s = params.name;
                                if (!s) {
                                    history.replace('/Sablony/' + data.rows[0].name);
                                    s = data.rows[0].name;
                                }
                                if (s) {
                                    const inx = data.rows.findIndex(row => row.name === s);
                                    const returnedRow = data.rows[inx];

                                    setSelectedTemplate(returnedRow);
                                    document.querySelector(`#template-table tbody tr:nth-child(${inx + 1})`)?.classList.add('selected');
                                }
                            }
                        }}
                        strictPerSize="100"
                        pushedFilters={[{
                            fieldName: 'active',
                            filterVal: 'a',
                            comparator: Comparator.EQ
                        }]}
                    />
                </Col>

                {!!selectedTemplate &&
                    <Col lg={8} key={selectedTemplate?.id}>
                        <div className="shadow-container justify-content-center flex-wrap">
                            <Row>
                                <Col>
                                    {isExistingTemplate && <Button onClick={hadnleDownloadTemplate}>
                                        <Icon icon="download"/> &nbsp; Stáhnout
                                    </Button>}
                                </Col>
                                <Col className="d-flex justify-content-end">
                                {isExistingTemplate && <Button variant="danger" onClick={handleRemoveTemplate}>
                                    <Icon icon="trash"/> &nbsp; Smazat Šablonu
                                </Button>}
                            </Col>
                            </Row>
                            <Form onSubmit={handleSubmit} ref={formRef} validated={validated} noValidate>
                                <HeadLineSeparator
                                    text={isExistingTemplate ? "Detail šablony" : 'Vytváření nové šablony'}/>
                                {isExistingTemplate && <Row>
                                    <Col className="text-center" ref={relationsTarget}>
                                        Počet relací s kurzy: <b>{selectedTemplate.Course.length}</b>
                                        <Icon icon="search evented"
                                              style={{fontSize: '0.8rem', cursor: 'pointer', marginLeft: 5}}
                                              title="Zobrazit relace" onClick={(() => setShowRelations(true))}/>
                                    </Col>
                                </Row>}
                                <AutoRow>
                                    <BInputLabeled
                                        name="name" label="Název"
                                        defaultValue={selectedTemplate.name}
                                        required/>
                                    <React.Fragment>
                                        <Form.Label>Typ dokumentu</Form.Label>
                                        <RSelect options={typeOptions}
                                                 defaultValue={typeOptions.find(o => o.value === selectedTemplate.type)}
                                                 name="type"
                                                 placeholder="Vyberte typ dokumentu"
                                                 openMenuOnFocus
                                                 required
                                        />
                                        <input type="hidden"/>
                                    </React.Fragment>
                                </AutoRow>

                                <Row style={{marginTop: 20}}>
                                    <Col className="d-flex justify-content-end gap-2">
                                        {isExistingTemplate ? <Button type="submit">
                                                Upravit šablonu
                                            </Button> :
                                            <Button onClick={(e) => {
                                                checkValidation(e, formRef.current);
                                            }}>
                                                Nahrát šablonu
                                            </Button>}
                                    </Col>
                                </Row>
                            </Form>
                            {isExistingTemplate && <>
                                <HeadLineSeparator text="Náhled"/>
                                <DocumentViewer
                                    fileModalHeader={`${selectedTemplate.name}.${selectedTemplate.file_type}`}
                                    //url={`https://praxistestik.tode.cz/templates/${selectedTemplate.file_name}.${selectedTemplate.file_type}`}/>
                                    url={`https://praxistestik.tode.cz/templates/praxe.docx`}/>
                            </>}
                        </div>
                    </Col>
                }

            </Row>
        </Container>

        {isExistingTemplate && selectedTemplate.Course?.length > 0 &&
            <SimpleOverlay show={showRelations} setShow={setShowRelations}
                           style={{maxWidth: 550}}
                           target={relationsTarget.current}
                           header="Kurzy, v kterých je šablona užívána"
            >
                <ul className="no-bullets">
                    {selectedTemplate.Course.map(course => {

                        return <Link to={`/Kurzy/${course.department}-${course.name}`} key={`${course.department}/${course.name}`}>
                            <li>
                                {`${course.department}/${course.name}`}
                            </li>
                        </Link>
                    })}
                </ul>
            </SimpleOverlay>}
    </div>;
};

export default Templates;