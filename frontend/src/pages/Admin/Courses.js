import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {useHistory, useParams} from "react-router-dom";
import axios from '../../hooks/auth-axios';
import {Col, Container, Form, Modal, Row} from "react-bootstrap";
import {ADMIN_COURSE_TABLE} from "../../defs/tables";
import {Comparator} from "react-bootstrap-table2-filter";
import Table from "../../components/Table";
import {addSelectedRowClass, generateUniqId} from "../../utils/Utils";
import BInputLabeled from "../../components/BInputLabeled";
import AutoRow from "../../components/AutoRow";
import Button from "react-bootstrap/Button";
import HeadLineSeparator from "../../components/Head-line-separator";
import Icon from "../../components/Icon";
import IconBg from "../../components/IconBg";
import {ContextMenu, MenuItem, ContextMenuTrigger} from "react-contextmenu";
import AuthContext from "../../store/auth-context";
import DocumentViewer from "../../components/Document-viewer";
import Dialog from "react-bootstrap-dialog";
import TemplateUploader from "../../components/TemplateUploader";
import draftToHtml from 'draftjs-to-html';
import { EditorState, convertToRaw, ContentState} from 'draft-js';
import htmlToDraft from 'html-to-draftjs';

import {Editor} from "react-draft-wysiwyg"
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import {toast} from "react-toastify";


const WordFile = props => {
    const {fileName, onClick} = props;
    return <div className="word-file text-center" title={fileName} onClick={onClick}>
        <IconBg icon="file-word" className="word-icon" title={fileName}/>

        <div className="file-name">
            {fileName}
        </div>
    </div>;
}

export const Courses = props => {
    const history = useHistory();
    const params = useParams();

    const authCtx = useContext(AuthContext);
    const userData = authCtx.userData;
    const confirmDialog = useRef();
    const templateUploaderRef = useRef();

    const [key, setKey] = useState(generateUniqId);
    const [formKey, setFormKey] = useState(generateUniqId);


    const [selectedCourse, setSelectedCourse] = useState(null);
    const [plusTemplate, setPlusTemplate] = useState(null);
    const [allTemplates, setAlltemplates] = useState([]);
    const [searchTemplate, setSearchTemplate] = useState('');
    const [templateToPreview, setTemplateToPreview] = useState(null);
    const [validated, setValidated] = useState(false);

    let editorState = EditorState.createEmpty();

    const [description, setDescription] = useState(editorState);
    const onEditorStateChange = (editorState) => {
        setDescription(editorState);
    }

    useEffect(() => {
        if(selectedCourse?.description){
            const contentBlock = htmlToDraft(selectedCourse?.description);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                setDescription(editorState);

            }
        }else{
            setDescription(EditorState.createEmpty());
        }
    },[selectedCourse, setDescription])

    const handleSubmenuItemClick = useCallback((e, data, target) => {
        if (data.action === 'Show') {
            setTemplateToPreview({fileFullName: `${data.template.file_name}.${data.template.file_type}`,name: data.template.name});
        } else if (data.action === 'Remove') {
            const newSelectedCourse = {...selectedCourse};
            newSelectedCourse.Template = newSelectedCourse.Template.filter(t => t.id !== data.template.id);
            setSelectedCourse(newSelectedCourse);

            handleCourseUpdate(selectedCourse.id, {
                Template: {
                    disconnect: {id: data.template.id}
                }
            });
        } else if (data.action === 'Add') {
            handleAddTemplateToCourse(data.template);
        } else if(data.action === 'Navigate') {
            history.push(`/Sablony/${data.template.name}`);
        }

    }, [selectedCourse]);

    const modalHide = () => {
        setPlusTemplate(null);
        setSearchTemplate('');
    };

    const handleCourseUpdate = useCallback((id, data) => {
        if (!!id && !!data) {
            return axios.patch(`./course`, {
                id,
                data: {...data, updated_by: userData.userName}
            })
        }
        return false;
    }, [userData]);

    const handleAddTemplateToCourse = useCallback((template) => {
        if (selectedCourse.Template.findIndex(t => t.id === template.id) === -1) {
            const newSelectedCourse = {...selectedCourse};
            newSelectedCourse.Template.push(template);
            setSelectedCourse(newSelectedCourse);
            modalHide();
            handleCourseUpdate(selectedCourse.id, {
                Template: {
                    connect: {id: template.id}
                }
            });

        }
    }, [selectedCourse, handleCourseUpdate]);

    const handleSubmit = useCallback(async e => {
        const form = e.currentTarget;

        e.preventDefault();
        if (form.checkValidity() === false) {
            e.stopPropagation();
            setValidated(true);
            return true;
        }


        const submittedData = Object.fromEntries(new FormData(e.target));
        submittedData.description = draftToHtml(convertToRaw(description.getCurrentContent()));

        let touchedFields = {};
        Object.entries(submittedData).forEach(([key, val]) => {
            if (selectedCourse[key] !== val) {
                if (key === 'approximate_days_for_completion') {
                    submittedData[key] = Number(val);
                }

                touchedFields[key] = submittedData[key];
            }
        });

        if (touchedFields !== {}) {
            if(selectedCourse?.id){
                let appendObj = {};
                const uniqueFieldWasChanged = touchedFields.hasOwnProperty('name') ||touchedFields.hasOwnProperty('department');
                if(uniqueFieldWasChanged){
                    appendObj = {
                        name: selectedCourse.name,
                        department: selectedCourse.department,
                    }
                }

                const response = await handleCourseUpdate(selectedCourse.id, {...appendObj, ...touchedFields});
                if(response.data.error_cs){
                    toast.error('Katedra+Název musí být unikátní!', {
                        position: "top-center",
                        autoClose: 2000,
                    });
                    setValidated(false);
                    return true;
                }

                const c = {...selectedCourse, ...touchedFields};
                setSelectedCourse(c);

                if(uniqueFieldWasChanged){
                    history.replace(`/Kurzy/${c.department}-${c.name}`);
                }
                toast.success('Kurz úspěšně upraven', {
                    position: "top-right",
                    autoClose: 2000,
                });
                setKey(generateUniqId());
            }else{
                const {data} = await axios.post('course', {
                    ...touchedFields,
                    created_by: userData.userName,
                });

                if(data.error_cs){
                    toast.error('Katedra+Název musí být unikátní!', {
                        position: "top-center",
                        autoClose: 2000,
                    });
                    setValidated(false);
                    return true;
                }
                history.replace(`/Kurzy/${data.department}-${data.name}`);
                setSelectedCourse(data);
                setKey(generateUniqId());
            }

            setValidated(false);
        }
    }, [selectedCourse, handleCourseUpdate, description]);

    const handleRemoveCourse = useCallback(async () => {
        const dialog = await new Promise((resolve, reject) => {
            confirmDialog.current.show({
                title: 'Upozornění',
                body: <>Opravdu si přejete smazat kurz <b>{`${selectedCourse.department}/${selectedCourse.name}`}</b>?</>,
                actions: [
                    Dialog.Action('Zrušit', () => resolve(false)),
                    Dialog.Action('Smazat', () => resolve(true), 'btn-danger')
                ],
                bsSize: 'small',
                onHide: (dialog) => {
                    dialog.hide()
                    resolve(false);
                }
            });
        });

        if(dialog === true){
            await axios.delete(`course/${selectedCourse.id}`);
            toast.success('Kurz byl úspěšně smazán', {
                position: "top-right",
                autoClose: 2000,
            });
            history.replace('/Kurzy');
            setSelectedCourse(null);
            setKey(generateUniqId());
        }

        },[selectedCourse, handleCourseUpdate]);


    useEffect(() => {
        axios.get(`./template`, {
            params: {
                orderBy: [{name: 'asc'}],
                filter: {
                    fieldName: 'active',
                    filterVal: 'a',
                    comparator: Comparator.EQ
                }
            }
        }).then(response => {
            setAlltemplates(response.data);
        });
    }, [])

    const isExistingCourse = !!selectedCourse?.id;

    return <div id="courses" key={key}>

        <Dialog ref={confirmDialog} />
        {!!templateToPreview &&
            <DocumentViewer fileModalHeader={templateToPreview.name}
                            url={`https://praxistestik.tode.cz/templates/${templateToPreview.fileFullName}`} inModal modalProps={{
                onRequestClose: () => {
                    setTemplateToPreview(null);
                }
            }}/>}
        {!!plusTemplate && <Modal show={!!plusTemplate && !templateToPreview} onHide={modalHide}>
            <Modal.Header>
                <h5>
                    {plusTemplate === 'c' ? "Vyberte smlouvu, kterou si přejete přidat" : "Vyberte cílový dokument, který si přejete přidat"}
                </h5>
            </Modal.Header>
            <Modal.Body>
                <Form.Control placeholder="Vyhledat šablonu podle názvu" value={searchTemplate}
                              onChange={e => setSearchTemplate(e.target.value)} style={{marginBottom: 30}}/>
                <Container className="d-flex flex-wrap gap-4 overflow-auto" style={{maxHeight: 345}}>
                    <TemplateUploader ref={templateUploaderRef} onAfterUpload={(createdTemplate) => {
                        setAlltemplates([...allTemplates, createdTemplate]);
                    }}/>
                    <IconBg icon="plus" className="add-plus-icon" title="Nahrát novou šablonu"
                            onClick={()=>{
                                templateUploaderRef.current.click(plusTemplate)
                            }}/>
                    {allTemplates
                        .filter(t => (t.type === plusTemplate) // get only contracts or final documents
                            && (!searchTemplate || (searchTemplate && t.name.toLowerCase().includes(searchTemplate.toLowerCase()))) // search
                            && selectedCourse.Template.findIndex(ct => ct.id === t.id) === -1) // exlude already added
                        .map(template => {
                            const fullName = `${template.name}.${template.file_type}`;
                            return <ContextMenuTrigger key={template.id} id="MODAL_MULTI"
                                                       name={fullName} holdToDisplay={100}
                                                       mouseButton={0}
                                                       collect={() => ({template})}
                            >
                                <WordFile key={template.id} fileName={fullName}/>
                            </ContextMenuTrigger>
                        })}
                </Container>
            </Modal.Body>
            <ContextMenu id="MODAL_MULTI" className="context-with-icons">
                <MenuItem onClick={handleSubmenuItemClick} data={{action: 'Add'}}>
                    <Icon icon="plus"/> Přidat
                </MenuItem>
                <MenuItem onClick={handleSubmenuItemClick} data={{action: 'Navigate'}}>
                    <Icon icon="pen"/> Upravit
                </MenuItem>
                <MenuItem onClick={handleSubmenuItemClick} data={{action: 'Show'}}
                          collect={props => console.log('aa', props)}>
                    <Icon icon="eye"/> Zobrazit
                </MenuItem>
            </ContextMenu>
        </Modal>}
        <Container>
            <Row>
                <Col>
                    <Button onClick={() => {
                        setSelectedCourse({})
                        history.replace('/Kurzy');
                        setFormKey(generateUniqId());
                    }}>
                        <Icon icon="plus"/> Vytvořit nový kurz
                    </Button>
                </Col>
            </Row>
            <Row className="flex-lg-nowrap flex-md-wrap">
                <Col md={10} lg={3} style={{minWidth: 300}}>
                    <Table
                        keyField="id"
                        id="course-table"
                        API="course/includeTemplates"
                        className="layout-fixed first-col-id"
                        columnsDef={ADMIN_COURSE_TABLE}
                        noDataLabel="Zatím nebyl vytvořen žádný kurz"
                        defaultOrderBy={[{created_at: 'desc'}]}
                        remote={{
                            filter: true,
                            pagination: true,
                            sort: true,
                        }}
                        showPaginator={true}
                        rowEvents={{
                            onClick: (e, row) => {
                                if (!(!!selectedCourse && selectedCourse.id === row.id)) {
                                    setSelectedCourse(row);
                                    addSelectedRowClass(e.target);
                                    setFormKey(generateUniqId());
                                    history.replace(`/Kurzy/${row.department}-${row.name}`);
                                }
                            }
                        }}
                        onDidMount={(data)=>{
                            if (data.hasOwnProperty('rows') && data.totalRows > 0) {
                                let s = params.name;
                                if (!s) {
                                    const course = data.rows[0];
                                    history.replace(`/Kurzy/${course.department}-${course.name}`);
                                    s = `${course.department}-${course.name}`;
                                }
                                if (s) {
                                    const inx = data.rows.findIndex(row => `${row.department}-${row.name}` === s);
                                    const returnedRow = data.rows[inx];

                                    setSelectedCourse(returnedRow);
                                    document.querySelector(`#course-table tbody tr:nth-child(${inx + 1})`)?.classList.add('selected');
                                }
                            }
                        }}
                        strictPerSize="100"
                        pushedFilters={[{
                            fieldName: 'active',
                            filterVal: 'a',
                            comparator: Comparator.EQ
                        }]}
                    />
                </Col>
                <Col md={!!selectedCourse ? 10 : 1} lg={9}>
                    {!!selectedCourse &&
                        <div className="shadow-container justify-content-center flex-wrap"
                             style={{padding: '30px 25px'}}>
                            {isExistingCourse && <Col className="d-flex justify-content-end">
                                <Button variant="danger" onClick={handleRemoveCourse}>
                                    <Icon icon="trash"/> &nbsp; Smazat kurz
                                </Button>
                            </Col>}
                            <Form key={formKey} onSubmit={handleSubmit} noValidate validated={validated}>
                                <HeadLineSeparator text={isExistingCourse ? "Detail kurzu" : "Vytváření nového kurzu"}/>
                                <AutoRow>
                                    <BInputLabeled name="department" label="Katedra"
                                                   defaultValue={selectedCourse.department} required/>
                                    <BInputLabeled name="name" label="Název"
                                                   defaultValue={selectedCourse.name}
                                                   required/>
                                    <BInputLabeled
                                        name="approximate_days_for_completion" label="Počet dnů pro splnění"
                                        type="number"
                                        min="1"
                                        valueasnumber={"Number(value)"}
                                        defaultValue={selectedCourse.approximate_days_for_completion}
                                        required/>
                                </AutoRow>
                                {isExistingCourse && <Row style={{marginTop: 20}}>
                                    <Col>
                                        <HeadLineSeparator text="Smlouvy"/>
                                        <Container className="d-flex flex-wrap gap-4">
                                            <IconBg icon="plus" className="add-plus-icon" title="Přidat smlouvu"
                                                    onClick={() => setPlusTemplate('c')}/>
                                            {isExistingCourse && selectedCourse.Template?.filter(t => t.type === "c").map(template => {
                                                const fullName = `${template.name}.${template.file_type}`;
                                                return <ContextMenuTrigger key={template.id} id="MULTI"
                                                                           name={fullName} holdToDisplay={100}
                                                                           mouseButton={0}
                                                                           collect={() => ({template})}
                                                >
                                                    <WordFile fileName={fullName}/>
                                                </ContextMenuTrigger>
                                            })}
                                        </Container>
                                    </Col>
                                    <Col>
                                        <HeadLineSeparator text="Cílové dokumenty"/>
                                        <Container className="d-flex flex-wrap gap-4">
                                            <IconBg icon="plus" className="add-plus-icon"
                                                    title="Přidat cílový dokument"
                                                    onClick={() => setPlusTemplate('f')}/>
                                            {selectedCourse.Template?.filter(t => t.type === "f").map(template => {
                                                const fullName = `${template.name}.${template.file_type}`;
                                                return <ContextMenuTrigger key={template.id} id="MULTI"
                                                                           name={fullName} holdToDisplay={100}
                                                                           mouseButton={0}
                                                                           collect={() => ({template})}
                                                >
                                                    <WordFile fileName={fullName}/>
                                                </ContextMenuTrigger>
                                            })}
                                        </Container>
                                    </Col>
                                </Row>}

                                {<Row style={{marginTop: 20}}>
                                    <div className="head-line-separator"><h4><span>Popis kurzu</span></h4></div>
                                    <Col style={{paddingLeft:20, paddingRight:20}}>
                                        <Editor
                                            wrapperClassName="course-editor"
                                            editorState={description}
                                            onEditorStateChange={onEditorStateChange}
                                        />
                                    </Col>
                                </Row>}

                                <Row style={{marginTop: 20}}>
                                    <Col className="d-flex justify-content-end">
                                        <Button type="submit">
                                            {isExistingCourse ? "Upravit kurz" : "Vytvořit kurz"}
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                            <ContextMenu id="MULTI" className="context-with-icons">
                                <MenuItem onClick={handleSubmenuItemClick} data={{action: 'Show'}}>
                                    <Icon icon="eye"/> Zobrazit
                                </MenuItem>
                                <MenuItem onClick={handleSubmenuItemClick} data={{action: 'Navigate'}}>
                                    <Icon icon="pen"/> Upravit
                                </MenuItem>
                                <MenuItem onClick={handleSubmenuItemClick} data={{action: 'Remove'}}>
                                    <span className="danger-color">
                                        <Icon icon="trash"/> Odebrat
                                    </span>
                                </MenuItem>
                            </ContextMenu>
                        </div>
                    }
                </Col>

            </Row>
        </Container>
    </div>
}

export default Courses;