import React, {useCallback, useEffect, useState} from 'react';
import Table from '../../components/Table';
import {Col, Container, Row} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import {useHistory} from "react-router-dom";
import {ADMIN_HOME_TABLE} from "../../defs/tables";
import {Form} from "react-bootstrap";
import {getCourseOptionsFromDb} from "../../defs/dataModel";
import Icon from "../../components/Icon";

const AdminHome = props => {
    const history = useHistory();
    const [hideCompleted, _setHideCompleted] = useState(window.localStorage.getItem('admin-main-table-show-completed'));

    const setHideCompleted = useCallback(val => {
        const stringedVal = String(val);
        window.localStorage.setItem('admin-main-table-show-completed', stringedVal);
        _setHideCompleted(stringedVal);
    }, []);

    const [courses, setCourses] = useState({});

    useEffect(() => {
        getCourseOptionsFromDb().then(response => setCourses(response));
    }, [])

    return <div id="admin-home">
        <Container className="actions d-flex justify-content-end">
            <Row>
                <Col className="d-inline-flex gap-2">
                    <Link to="/Sablony">
                        <Button>
                            <Icon icon="file"/> &nbsp; Šablony
                        </Button>
                    </Link>
                    <Link to="/Kurzy">
                        <Button>
                          <Icon icon="wrench"/> &nbsp; Spravovat kurzy
                        </Button>
                    </Link>
                </Col>
            </Row>
        </Container>
        <Container className="intership-table-container">
            <Table
                key={hideCompleted}
                keyField="id"
                API="internship"
                columnsDef={ADMIN_HOME_TABLE(courses)}
                noDataLabel="Zatím nebyla vytvotena žádná praxe"
                defaultOrderBy={[{created_at: 'desc'}, {state: 'desc'}, {id: 'desc'}]}
                remote={{
                    filter: true,
                    pagination: true,
                    sort: true,
                }}
                showPaginator={true}
                rowEvents={{
                    onDoubleClick: (e, row) => {
                        history.push(`/detail-praxe/${row.id}`);
                    }
                }}
                pushedFilters={(hideCompleted == 'true') ? [{
                    fieldName: 'state',
                    filterVal: 'c',
                    comparator: '!='
                }] : null}
                topComponent={() => {
                    return <Container className="d-inline-flex justify-content-left" style={{paddingLeft: 10}}>
                        <Form.Check
                            type="switch"
                            label="Skrýt splněné praxe"
                            onChange={e => setHideCompleted(e.target.checked)}
                            checked={hideCompleted === 'true'}
                        />
                    </Container>;
                }}
            />
        </Container>
    </div>;
};

export default AdminHome;