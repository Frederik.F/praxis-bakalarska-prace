import {Link} from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import logo from '../images/logo_UJEP_white.png';
import Icon from "../components/Icon";


export const Welcome = () => {

    return <div id="welcome-page">
        <div className="welcome-banner">
            <h1>
                <img src={logo} alt="ujep.logo"/>
            </h1>
            <Link to='/'>
                <Button variant="light">
                    <Icon icon="sign-out-alt" />&nbsp;
                    Vstup do systému Praxis
                </Button>
            </Link>
        </div>

    </div>;
}

export default Welcome;