## Setup
- V kořenovém adresáři spustte příkaz `yarn install`
- V souboru `src/config.js` nastavte proměnnou `API_BASE_URL` na URL vašeho serveru v podobně: `URL_SERVERU/API/v1` 
## Dostupné skripty
### `yarn start`

Spustí aplikaci v režimu vývoje.\
Otevře [http://localhost:3000](http://localhost:3000) ve výchozím prohlížeči.

Stránka se znovu načte, pokud provedete úpravy.\
V konzoli se také zobrazí případné chyby.


### `yarn build`
Sestaví aplikaci pro produkční verzi do složky `build`.\
Správně sestaví React v produkčním režimu a optimalizuje sestavení pro dosažení nejlepšího výkonu.

Sestavení je minifikováno a názvy souborů obsahují hashe.\

Další informace naleznete v části o [deployment](https://facebook.github.io/create-react-app/docs/deployment).
