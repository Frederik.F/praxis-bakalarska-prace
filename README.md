# Praxis - Bakalářská práce

Veřejné přílohy k bakalářské práci Praxis. 

Nachází se zde uživatelská příručka aplikace a zdrojové kódy frontendu i backendu. 

Zdrojové kódy nejsou úplné. Chybí údaje pro připojení k databázi a jiná citlivá data.

Doporučuji se podívat na komentováné demonstrativní video aplikace Praxis, které je k dipozici v sekci "Screenshoty a demonstrativní video", v kterém jsou popsané funkcionality této aplikace. 

# Název projektu
Praxis - Webová aplikace pro správu odborných praxí

## Demo odkaz:
Demo ukázka aplikace [https://ujep-praxis.herokuapp.com/](https://ujep-praxis.herokuapp.com/)

Bohužel, zvolený hosting neumožnujě plné otestování aplikace z následujích důvodů:
1. Webový server hosting po delší neaktivitě sám vypiná. Je tedy při delší neaktivitě potřeba aplikaci načíst, čímž se interně spustí webový server a následně aplikaci refreshnout.
2. Hosting automaticky po čase maže nahrané soubory, které nejsou přidané v repozitáři tohoto projektu. To má za následek, že soubory nahraných dokumentů v demo projektu tedy existují pouze dočasně.
3. Aplikace je značně pomalejší. To je způsobeno především využití externího poskytovate MySQL databáze (heroku má tuto službu zpoplatněnou) a další limitující faktory předplatného, které je zdarma.


## O aplikaci
Praxis je aplikace, která primárně zajištujě generování veškerých dokumentů spojených s realizací odborné praxe na univerzite, a to za
pomocí dat získaných z IS STAG a veřejného registru firem. Následně sleduje plnění dílčích
kroků a průběh samotné praxe. Zajištujě jednodušší správu praxí a potřebných dokumentů. 

## Screenshoty a demonstrativní video
Video: [https://youtu.be/JnhxiWl1xlw](https://youtu.be/JnhxiWl1xlw)

Odkaz na album: [https://photos.app.goo.gl/fzjwf9dGKEGs9kM19](https://photos.app.goo.gl/fzjwf9dGKEGs9kM19)

Pořízeno autorem práce

## Technologie
Využité techonologie: `HTML`, `JS`, `ReactJS`, `NodeJs`, `Express.js`, `LESS`, `CSS`

## Setup
## Frontend
- Po stažení nebo naklonování repozitáře, přejděte do adresáře `frontend`.
- V tomto adresáři spustte příkaz `yarn install`.
- V `src/config.js` nastavte proměnnou `API_BASE_URL` na URL vašeho serveru v podobně: `URL_SERVERU/API/v1` 
## Dostupné skripty
### `yarn start`

Spustí aplikaci v režimu vývoje.\
Otevře [http://localhost:3000](http://localhost:3000) ve výchozím prohlížeči.

Stránka se znovu načte, pokud provedete úpravy.\
V konzoli se také zobrazí případné chyby.


### `yarn build`
Sestaví aplikaci pro produkční verzi do složky `build`.\
Správně sestaví React v produkčním režimu a optimalizuje sestavení pro dosažení nejlepšího výkonu.

Sestavení je minifikováno a názvy souborů obsahují hashe.\

Další informace naleznete v části o [deployment](https://facebook.github.io/create-react-app/docs/deployment).


## Backend
- Stáhněte nebo naklonujte repozitář a přejděte do adresáře `backend`.
- V tomto adresáři spustte příkaz `yarn install`
- V adresáři vytvořte soubor `.env`, který bude obsahovat připojení na databázi: `DATABASE_URL=""`

## Dostupné skripty
### `nodemon .`
Spustí server v režimu vývoje na adrese [http://localhost:3331](http://localhost:3331).\
Server se bude automaticky přenačítat, po provedení úpravy.

## Status
Praxis je stále ve vývoji. `Verze 2.0` výjde po prvním pilotním testování aplikace.
