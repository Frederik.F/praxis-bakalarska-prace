const express = require("express");
const cors = require("cors");
const userRouter = require('./routes/user.route');
const templateRouter = require('./routes/template.route');
const aresRouter = require('./routes/ares.route');
const studentRouter = require('./routes/student.route');
const internshipRouter = require('./routes/internship.route');
const courseRouter = require('./routes/course.route');
const fileUpload = require('express-fileupload');


// Init express
const app = express();

// parse requests of content-type: application/json
// parses incoming requests with JSON payloads
app.use(express.json());
// enabling cors for all requests by using cors middleware
app.use(cors());
// Enable pre-flight
app.options("*", cors());

app.use(fileUpload());


app.use(`/api/v1/users`, userRouter);
app.use(`/api/v1/template`, templateRouter);
app.use('/api/v1/ares', aresRouter)
app.use('/api/v1/students', studentRouter);
app.use('/api/v1/internship', internshipRouter);
app.use('/api/v1/course', courseRouter);

// starting the server
app.listen(3331, () =>
    console.log(`Server running on port ${3331}!`));


module.exports = app;