const axios = require("axios");
const HttpException = require("../utils/HttpException.utils");
const schoolsUrl = require("../utils/schools-url");

async function getStagService(url, req, noTokenNeeded = false) {
    const wscookie = req.headers.wscookie;
    const schoolUrl = req.headers.schoolurl;
    const params = req.query;
    const school = 'demo';

    if ((!wscookie || wscookie === 'null') && !noTokenNeeded) {
        throw new HttpException(401, 'token is require');
    }

    let query = '';

    if(params && Object.values(params).length > 0){
        query = `?${Object.keys(params).map(key => key + '=' + params[key]).join('&')}`;
    }

    let finalUrl = url;
    if (school && !url.startsWith('http') ) {
        finalUrl = `${schoolUrl || (schoolsUrl[school])}${url}${query}`;
    }else{
        finalUrl = `${finalUrl}${query}`
    }

    //console.log('finalUrl',finalUrl);
    const respond = await axios.get(finalUrl, {
        headers: {
            cookie: `WSCOOKIE=${wscookie}`
        }
    }).catch(err => {
        throw new HttpException(400, err);
    });

    return respond.data;
}

module.exports = getStagService;