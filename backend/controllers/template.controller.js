const {tableOptions, uploadFile, removeFile} = require('../utils/common.utils');
const { PrismaClient } = require('@prisma/client');
const {dirname} = require("path");
const prisma = new PrismaClient();

/******************************************************************************
 *                              Template Controller
 ******************************************************************************/
class TemplateController {
    getAllTemplates = async (req, res) => {
        const options = tableOptions(req);

        const templates = await prisma.Template.findMany(options);

        res.json(templates);
    };

    getTemplatesForTable = async (req, res) => {
        const options = tableOptions(req);
        const rows = await prisma.Template.findMany(options);

        const totalRows = await prisma.Template.aggregate({
            where: options.where,
            _count: true
        });

        res.json({
            rows,
            totalRows: totalRows._count
        });
    }

    getTemplateById = async (req, res, next) => {
        return await prisma.Template.findUnique({
            id: Number(req.params.id)
        });
    };

    getTemplateFile = async (req, res, next) => {
        const template = await prisma.Template.findUnique({
            where: { id: Number(req.params.id)}
        });

        const file = dirname(require.main.filename) + `/uploads/template/${template.file_name}.${template.file_type}`;
        res.sendFile(file);
    };

    getTemplatesForTableWithCourses = async (req, res) => {
        const options = tableOptions(req);
        options.include = {
            Course: {
                where:{
                    active: 'a'
                }
            }
        };

        const rows = await prisma.Template.findMany(options);

        const totalRows = await prisma.Template.aggregate({
            where: options.where,
            _count: true
        });

        res.json({
            rows,
            totalRows: totalRows._count
        });
    }


    createTemplate = async (req, res) => {
        const isUnique = await prisma.Template.findUnique({
            where: {
                name_active: {
                    name: req.body.name,
                    active: 'a'
                }
            }
        });

        if(isUnique !== null){
            res.json({error_cs: "Název praxe musí být unikátní!"});
            return true;
        }

        const uploadResult = uploadFile(req, '/uploads/template/', 'templateFile');

        const data = {...req.body};

        if(!data.name || data.name === "undefined"){
            data.name = uploadResult.fileName;
        }


        const createdTemplate = await prisma.Template.create({
           data:{
               file_name: uploadResult.fileName,
               file_type: uploadResult.fileType,
               ...data
           }
        });

        res.json(createdTemplate);
    };

    updateTemplate = async (req, res) => {
        if(req.body.data.name){
            const isUnique = await prisma.Template.findUnique({
                where: {
                    name_active: {
                        name: req.body.data.name,
                        active: 'a'
                    }
                }
            });

            if(isUnique !== null){
                res.json({error_cs: "Název praxe musí být unikátní!"});
                return true;
            }
        }

        const updatedTemplate = await prisma.Template.update({
            where: {
                id: Number(req.body.id),
            },
            data: {
                ...req.body.data
            },
        });

        res.json(updatedTemplate);
    }


    deleteTemplate = async (req, res) => {
        let response = {};
        const deletedTemplate = await prisma.Template.findUnique({
            select: {
                file_name: true,
                file_type: true,
                name: true,
                Course: true
            },
            where:{
                id: Number(req.params.id),
            }
        });

        if(deletedTemplate.Course.length === 0){
            const removed = removeFile(`/uploads/template/${deletedTemplate.file_name}.${deletedTemplate.file_type}`);

            if(removed){
                response = await prisma.Template.delete({
                   where:{
                       id: Number(req.params.id),
                   }
                });
            }

        }else{
            const alredyDeletedCount = await prisma.Template.aggregate({
                where: {
                    name: {startsWith: deletedTemplate.name},
                    active: 'd'
                },
                _count: true
            });

            response = await prisma.Template.update({
                where: {
                    id: Number(req.params.id),
                },
                data: {
                    name: deletedTemplate.name + `(deleted${alredyDeletedCount._count > 0 ? `(${alredyDeletedCount._count++})` : ''})`,
                    active: 'd'
                },
            });

        }

        res.json(response);
    };

}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new TemplateController;