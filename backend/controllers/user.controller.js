const UserModel = require('../models/user.model');
const stagService = require('../services/StagService');


/******************************************************************************
 *                              User Controller
 ******************************************************************************/
class UserController {

    getCurrentUser = async (req, res, next) => {
        const currentUser = await UserModel.getCurrentUser(req);
        res.json(currentUser);
    };

    logout = async (req, res, next) => {
        const repsond = await stagService(`ws/services/rest2/help/invalidateTicket?ticket=${req.headers.wscookie}`, req);

        res.json(repsond);
    }
}


/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new UserController;