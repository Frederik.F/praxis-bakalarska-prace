const Role = require("../utils/userRoles.utils");
const {PrismaClient} = require('@prisma/client');
const {tableOptions} = require("../utils/common.utils");
const prisma = new PrismaClient();

/******************************************************************************
 *                              Internship Controller
 ******************************************************************************/
class InternshipController {
    getAllInternships = async (req, res) => {
        const options = tableOptions(req);

        // if its not admin returns only records that has field student_id equals to current user userName
        if(!Role.Admin.includes(req.currentUser.role)){
            if(!options.hasOwnProperty('where')){
                options.where = {student_num: req.currentUser.userName};
            }else{
                options.where.AND.push({student_num: req.currentUser.userName});
            }
        }

        const internships = await prisma.Internship.findMany(options);
        res.json(internships);
    };

    getInternshipForTable = async (req, res) => {
        const options = tableOptions(req);
        if (options.hasOwnProperty('where') && options.where.hasOwnProperty('AND')) {

            // determinate specific type of state which depends on internship date
            options.where.AND.forEach((condition, inx, arr) => {
                if (!!condition.state && ['z', 'p', 'd'].includes(condition.state.equals)) {
                    const originalState = condition.state.equals;
                    arr[inx].state.equals = 'i';
                    if (originalState === 'z') {
                        arr.push({
                            start_date: {gt: new Date()}
                        });
                    } else if (originalState === 'p') {
                        arr.push({
                            AND: [
                                {
                                    start_date: {lte: new Date()}
                                },
                                {
                                    end_date: {gte: new Date()}
                                }
                            ]
                        });
                    } else if (originalState === 'd') {
                        arr.push({
                            end_date: {lt: new Date()}
                        });
                    }
                }
            });
        }

        // if its not admin returns only records that has field student_id equals to current user userName
        if(!Role.Admin.includes(req.currentUser.role)){
            if(!options.hasOwnProperty('where')){
                options.where = {student_num: req.currentUser.userName};
            }else{
                options.where.AND.push({student_num: req.currentUser.userName});
            }
        }


        const internships = await prisma.Internship.findMany(options);

        const totalRows = await prisma.Internship.aggregate({
            where: options.where,
            _count: true
        });

        res.json({
            rows: internships,
            totalRows: totalRows._count
        });
    }

    getInternshipById = async (req, res, next) => {
        try {
            const options = {
                where: {
                    id: Number(req.params.id)
                }
            };
            if(!Role.Admin.includes(req.currentUser.role)){
                options.where.student_num = req.currentUser.userName;
            }

            const internships = await prisma.Internship.findFirst(options);
            res.json(internships);
        } catch (e) {
            res.json()
        }
    };

    getInternshipByIdWithTemplatesAndCourse = async (req, res, next) => {
        try {
            const options = {
                include: {
                    Template: {
                        select: {
                            id: true,
                            type: true,
                            file_name: true,
                            file_type: true,
                            name: true
                        },
                        where:{
                            active: 'a'
                        }
                    },
                    course: {
                        select:{
                            id: true,
                            approximate_days_for_completion: true
                        }
                    }
                },
                where: {
                    id: Number(req.params.id)
                }
            };
            if(!Role.Admin.includes(req.currentUser.role)){
                options.where.student_num = req.currentUser.userName;
            }

            const internships = await prisma.Internship.findFirst(options);
            res.json(internships);
        } catch (e) {
            res.json()
        }
    };

    createInternship = async (req, res) => {
        const {
            student_id,
            course,
            student_num,
            spolecnost_ico,
            praxe_zacatek,
            praxe_konec,
            spolecnost_povereni,
            created_by
        } = req.body;

        // get all templates that are connected with internship
        const templates = await prisma.$queryRaw`SELECT B AS id FROM _CourseToTemplate WHERE A = ${course}`;

        const internship = await prisma.Internship.create({
            data: {
                student_id,
                student_num,
                ico: Number(spolecnost_ico),
                start_date: new Date(praxe_zacatek),
                end_date: new Date(praxe_konec),
                supervisor: spolecnost_povereni,
                created_by,
                course: {
                    connect: {id: course}
                },
                Template: {
                    connect: templates
                }
            }
        });

        res.send(internship);
    };

    updateInternship = async (req, res) => {
        const id = Number(req.params.id);
        if (isNaN(id)) {
            res.json({code: 500, error: "wrong id"})
        }

        const {state, updated_by} = req.body;
        const internship = await prisma.Internship.update({
            where: {
                id
            },
            data: {
                state,
                updated_by
            }
        });

        res.json({status: 'success', data: internship});
    }

    getCountOfAllRecords = async (req, res) => {

    }

}


/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new InternshipController;