const studentModel = require('../models/student.model');


/******************************************************************************
 *                              User Controller
 ******************************************************************************/
class StudentController {

    getStudentInfo = async (req, res, next) => {
        const info = await studentModel.info(req);
        res.send(info);
    };

    getStudentCourses = async (req, res, next) => {
        let courses = await studentModel.getCourses(req);
        if(!!courses){
           courses = courses.predmetAbsolvoval;
        }
        res.send(courses);
    }

}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new StudentController;