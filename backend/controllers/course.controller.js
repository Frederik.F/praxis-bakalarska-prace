const { PrismaClient } = require('@prisma/client');
const {tableOptions} = require("../utils/common.utils");
const prisma = new PrismaClient();

/******************************************************************************
 *                              Course Controller
 ******************************************************************************/
class CourseController {
    getAllCourses = async (req, res) => {
        const options = tableOptions(req);

        const courses = await prisma.Course.findMany(options);

        res.json(courses);
    };

    getCoursesForTable = async (req, res) => {
        const options = tableOptions(req);
        const rows = await prisma.Course.findMany(options);

        const totalRows = await prisma.Course.aggregate({
            where: options.where,
            _count: true
        });

        res.json({
            rows,
            totalRows: totalRows._count
        });
    }

    getCoursesForTableWithTemplates = async (req, res) => {
        const options = tableOptions(req);
        options.include = {
            Template: {
                where:{
                    active: 'a'
                }
            }
        };

        const rows = await prisma.Course.findMany(options);

        const totalRows = await prisma.Course.aggregate({
            where: options.where,
            _count: true
        });

        res.json({
            rows,
            totalRows: totalRows._count
        });
    }

    updateCourse = async (req, res) => {
        const {name, department} = req.body.data;

        if(name || department){
            const isUnique = await prisma.Course.findUnique({
                where: {
                    course_name_course_department_course_active : {
                        name,
                        department,
                        active: 'a'
                    }
                }
            });

            if(isUnique !== null){
                res.json({error_cs: "Název praxe musí být unikátní!"});
                return true;
            }
        }

        const updateCourse = await prisma.Course.update({
            where: {
                id: Number(req.body.id),
            },
            data: {
                ...req.body.data
            },
        });

        res.json(updateCourse);
    }

    createCourse = async (req, res) => {
        const {name, department} = req.body;
        const isUnique = await prisma.Course.findUnique({
            where: {
                name_department_active : {
                    name,
                    department,
                    active: 'a'
                }
            }
        });

        if(isUnique !== null){
            res.json({error_cs: "Katedra + Název praxe musí být unikátní!"});
            return true;
        }


        const createdCourse = await prisma.Course.create({
            data: {
                ...req.body,
                name: req.body.name.toUpperCase(),
                department: req.body.department.toUpperCase(),
            }
        });

        res.json(createdCourse);
    }

    getCourseById = async (req, res) => {
        const course = await prisma.Course.findUnique({
            where:{
                id: Number(req.params.id)
            }
        });

        res.json(course);
    };

    deleteCourse = async (req, res) => {
        let response;
        const deletedCourse = await prisma.Course.findUnique({
            select: {
                name: true,
                department: true,
                Internship: true
            },
            where:{
                id: Number(req.params.id),
            }
        });

        if(deletedCourse.Internship.length === 0){
            response = await prisma.Course.delete({
                where:{
                    id: Number(req.params.id),
                }
            });

        }else{
            const alredyDeletedCount = await prisma.Course.aggregate({
                where: {
                    name: {startsWith: deletedCourse.name},
                    department: deletedCourse.department,
                    active: 'd'
                },
                _count: true
            });

            response = await prisma.Course.update({
                where: {
                    id: Number(req.params.id),
                },

                data: {
                    name: deletedCourse.name + `(deleted${alredyDeletedCount._count > 0 ? `(${alredyDeletedCount._count++})` : ''})`,
                    active: 'd'
                },
            });

        }

        res.json(response);
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new CourseController;