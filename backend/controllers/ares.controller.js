const HttpException = require('../utils/HttpException.utils');
const axios = require("axios");
const parseString = require('xml2js').parseString;
const {PrismaClient} = require('@prisma/client');
const prisma = new PrismaClient();


/******************************************************************************
 *                              ARES Controller
 ******************************************************************************/
class AresController {

    getCompanyDataByIco = async (req, res, next) => {
        const ico = req.params.ico;
        let saveCompanyToDatabase = false;

        if (!ico) {
            throw new HttpException(401, 'ico is requred!')
        }

        let data = await prisma.Company.findUnique({
            where: {
                ico: ico.replace(/\D/g, "")
            }
        });

        if (data && data.data_json) {
            res.send(data.data_json);
            return;
        }


        const ares = await axios.get(`http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=${ico}`);
        data = ares.data;
        saveCompanyToDatabase = true;

        //TODO make better alg to get data from ARES
        const clearValueByKey = (key, value) => {
            if (['ICO', 'OF', 'DIC'].includes(key)) {
                return value[0]['_']
            } else if (key === 'DV') {
                return value[0];
            } else if (key === 'AD' && Array.isArray(value)) {
                return {
                    UC: value[0]['D:UC'][0],
                    PB: value[0]['D:PB'][0],
                };
            } else if (key === 'AA' && Array.isArray(value)) {
                const obj = {};
                //console.log(value[0]);
                Object.keys(value[0]).forEach(key => {
                    if (key.startsWith('D:') && key !== 'D:AU') {
                        obj[key.split(':')[1]] = value[0][key][0];
                    }
                });
                return obj;
            }

            //return value;
        }

        // parse to JSON
        const final = await new Promise((resolve, reject) => parseString(data, (err, result) => {
            if (err) {
                reject(err);
            }
            const error = result['are:Ares_odpovedi']['are:Odpoved'][0]['D:E'];
            if (error) {
                res.status(204);
                res.send(result['are:Ares_odpovedi']['are:Odpoved'][0]['D:E'][0]['D:ET']);
                return false;
            }

            const realData = result['are:Ares_odpovedi']['are:Odpoved'][0]['D:VBAS'][0];
            const final = {};
            //console.log('result',JSON.stringify(result));
            Object.entries(realData).forEach(([nastyKey, nastyValue]) => {
                const key = nastyKey.split(':')[1];
                const value = clearValueByKey(key, nastyValue);
                if (value) {
                    final[key] = value;
                }
            });

            if (saveCompanyToDatabase) {
                prisma.Company.create({
                    data: {
                        ico: final.ICO,
                        name: final.OF,
                        data_json: final
                    }
                }).then(()=>{
                    resolve(final);
                }).catch(err => {
                    console.log(err);
                    resolve(final);
                });
            }
        }));

        res.send(final);
    }

    findCompanyByEverything = async (req, res, next) => {
        const searchedValue = req.params.searched_value;
        if (!searchedValue) {
            res.send(null);
        }
        let data = await prisma.Company.findMany({
            where: {
                OR: [
                    {name: {contains: searchedValue}},
                    {ico: {contains: searchedValue}},
                ]
            }
        });

        res.send(data);

    }
}

/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new AresController();