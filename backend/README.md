## Setup
- V kořenovém adresáři spustte příkaz `yarn install`
- V kořenovém adresáři vytvořte soubor `.env`, který bude obsahovat připojení na databázi: `DATABASE_URL=""`

## Dostupné skripty
### `nodemon .`
Spustí server v režimu vývoje na adrese [http://localhost:3331](http://localhost:3331).\
Server se bude automaticky přenačítat, po provedení úpravy.
