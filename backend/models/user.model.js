const getStagService = require('../services/StagService');
const HttpException = require("../utils/HttpException.utils");
const schoolsUrl = require("../utils/schools-url");
const axios = require("axios");

class UserModel {

    getCurrentUser = async (req) => {
        const url = 'ws/services/rest2/help/getStagUserForActualUser';
        const wscookie = req.headers.wscookie;
        const schoolUrl = req.headers.schoolurl;
        const school = 'demo';

        if ((!wscookie || wscookie === 'null')) {
            throw new HttpException(401, 'token is require');
        }

        let finalUrl = `${schoolUrl || (schoolsUrl[school])}${url}`;

        const respond = await axios.get(finalUrl, {
            headers: {
                cookie: `WSCOOKIE=${wscookie}`
            }
        }).catch(err => {
            throw new HttpException(400, err);
        });

        return respond.data;

    }

    getStudentInfo = async (req) => {
        return getStagService('ws/services/rest2/student/getStudentInfo', req);
    }

}


module.exports = new UserModel;