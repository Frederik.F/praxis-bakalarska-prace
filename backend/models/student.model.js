const getStagService = require('../services/StagService');

class StudentModel {

    info = async (req) => {
       return await getStagService('ws/services/rest2/student/getStudentInfo', req);
    }

    getCourses = async (req) => {
        return await getStagService('ws/services/rest2/student/getStudentPredmetyAbsolvoval', req);
    }

}

module.exports = new StudentModel;