const HttpException = require('../utils/HttpException.utils');
const UserModel = require('../models/user.model');

const auth = (roles) => {
    return async function (req, res, next) {
        try {

            if (!req.headers.wscookie) {
                throw new HttpException(401, 'Access denied. No credentials sent!');
            }

            const user = await UserModel.getCurrentUser(req);

            if (!user) {
                throw new HttpException(401, 'Authentication failed!');
            }


            // if the user role don't have the permission to do this action.
            // the user will get this error
            if (roles?.length && !roles.includes(user.role)) {
                throw new HttpException(401, 'Unauthorized');
            }

            // if the user has permissions
            req.currentUser = user;
            next();

        } catch (e) {
            e.status = 401;
            next(e);
        }
    }
}

module.exports = auth;