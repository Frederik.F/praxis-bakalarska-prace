const express = require('express');
const router = express.Router();
const internshipController = require('../controllers/internship.controller');
const auth = require('../middleware/auth.middleware');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');
const Role = require("../utils/userRoles.utils");

router.get('/',  auth(), awaitHandlerFactory(internshipController.getAllInternships));
router.get('/table',  auth(), awaitHandlerFactory(internshipController.getInternshipForTable));
router.get('/find/:id',  auth(), awaitHandlerFactory(internshipController.getInternshipById));
router.get('/find/withTemplatesAndCourse/:id',  auth(), awaitHandlerFactory(internshipController.getInternshipByIdWithTemplatesAndCourse));

router.post('/',  auth(), awaitHandlerFactory(internshipController.createInternship));

router.patch('/:id',   auth(Role.Admin), awaitHandlerFactory(internshipController.updateInternship));
//router.delete('/id/:id', auth(Role.Admin), awaitHandlerFactory(internshipController.deleteInternship));


module.exports = router;

