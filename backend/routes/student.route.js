const express = require('express');
const router = express.Router();
const studentController = require('../controllers/student.controller');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

router.get('/getInfo', awaitHandlerFactory(studentController.getStudentInfo));
router.get('/getCourses', awaitHandlerFactory(studentController.getStudentCourses));

module.exports = router;