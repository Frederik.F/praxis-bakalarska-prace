const express = require('express');
const router = express.Router();
const courseController = require('../controllers/course.controller');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');
const auth = require("../middleware/auth.middleware");
const Role = require("../utils/userRoles.utils");

router.get('/',  auth(), awaitHandlerFactory(courseController.getAllCourses));
router.get('/table',  auth(), awaitHandlerFactory(courseController.getCoursesForTable));
router.get('/table/includeTemplates',  auth(),awaitHandlerFactory(courseController.getCoursesForTableWithTemplates));
router.get('/find/:id', auth(), awaitHandlerFactory(courseController.getCourseById));
router.post('/', auth(Role.Admin), awaitHandlerFactory(courseController.createCourse));
router.patch('/',  auth(Role.Admin), awaitHandlerFactory(courseController.updateCourse));
router.delete('/:id',  auth(Role.Admin), awaitHandlerFactory(courseController.deleteCourse));


module.exports = router;

