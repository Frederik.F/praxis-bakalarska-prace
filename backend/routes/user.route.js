const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

router.get('/currentUser', awaitHandlerFactory(userController.getCurrentUser));

router.get('/logout', awaitHandlerFactory(userController.logout));
module.exports = router;