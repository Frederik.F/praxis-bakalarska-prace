const express = require('express');
const router = express.Router();
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');
const aresController = require('../controllers/ares.controller')
const HttpException = require("../utils/HttpException.utils");

router.get('/', () => {
    throw new HttpException(401, 'ico is requred! Try ares/ico')
});

router.get('/:ico', awaitHandlerFactory(aresController.getCompanyDataByIco));
router.get('/search/:searched_value', awaitHandlerFactory(aresController.findCompanyByEverything));

module.exports = router;