const express = require('express');
const router = express.Router();
const templateController = require('../controllers/template.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

router.get('/',  auth(), awaitHandlerFactory(templateController.getAllTemplates));
router.get('/table',  auth(), awaitHandlerFactory(templateController.getTemplatesForTable));
router.get('/table/includeCourses',  auth(), awaitHandlerFactory(templateController.getTemplatesForTableWithCourses));
router.get('/find/:id',  auth(), awaitHandlerFactory(templateController.getTemplateById));
router.get('/file/:id',  auth(), awaitHandlerFactory(templateController.getTemplateFile));

router.post('/',  auth(Role.Admin), awaitHandlerFactory(templateController.createTemplate));

router.patch('/',  auth(Role.Admin), awaitHandlerFactory(templateController.updateTemplate));
router.delete('/:id',  auth(Role.Admin), awaitHandlerFactory(templateController.deleteTemplate));


module.exports = router;