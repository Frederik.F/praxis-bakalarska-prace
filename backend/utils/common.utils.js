const axios = require("axios");
const schoolsUrl = require('../utils/schools-url');
const HttpException = require("../utils/HttpException.utils");
const fs = require('fs');
const {dirname} = require('path');
const moment = require('moment');

exports.formatDateDb = date => {
    return this.formatDate(date, 'YYYY-MM-DD');
}

exports.formatDateTimeDb = date => {
    return this.formatDate(date, 'YYYY-MM-DD HH:mm:ss');
}

exports.formatDate = (date, dateFormat = null) => {
    if (date === null) {
        return '';
    }

    if (typeof date === 'string' || !moment.isMoment(date)) {
        //console.log('DATE JE STRING', date);
        //date = parseISO(date, 'yyyy-mm-dd');
        date = moment(date);
    }

    return date.format(dateFormat);

    //return format(date, dateFormat);
}

const tableResolveFilters = filter => {
    if (filter) {
        let filterObj = JSON.parse(filter);
        if (filterObj) {
            if (!(filterObj instanceof Array)) {
                filterObj = [filterObj];
            }

            const AND = filterObj.map(filter => {
                const fieldType = filter.fieldType;
                let com = null;
                let val = filter.filterVal;
                if (filter.comparator === '=') {
                    com = 'equals';
                } else if (filter.comparator === 'LIKE') {
                    com = 'contains';
                } else if (filter.comparator === '>') {
                    com = 'gt';
                } else if (filter.comparator === '<') {
                    com = 'lt';
                } else if (filter.comparator === '>=') {
                    com = 'gte';
                } else if (filter.comparator === '<=') {
                    com = 'lte';
                } else if (filter.comparator === '!=') {
                    com = 'not';
                }

                if (['DATE', 'DATETIME'].includes(fieldType)) {
                    val = new Date(val);
                }

                if (filter.dataType === "NUMBER") {
                    val = Number(val);
                }

                if (!!com && !!val && !!filter.fieldName) {
                    return {
                        [(filter.filterReplaceName || filter.fieldName)]: {[com]: val}
                    };
                }

            });

            if (AND.length > 0 && AND[0] !== undefined)
                return {AND};

            return null;
        }
    }
}

exports.tableResolveFilters = tableResolveFilters;

exports.tableOptions = (req) => {
    const optObject = {};
    let {orderBy, filter, include, take, skip} = req.query;

    if (orderBy) {
        optObject.orderBy = JSON.parse(orderBy);
    }

    const resolvedFilters = tableResolveFilters(filter);
    if (resolvedFilters) {
        optObject.where = resolvedFilters;
    }

    if (!!take) optObject.take = Number(take);
    if (!!skip) optObject.skip = Number(skip);


    if (include) {
        optObject.include = JSON.parse(include);
    }

    return optObject;
}


exports.getPlaceholderStringForArray = (arr) => {
    if (!Array.isArray(arr)) {
        throw new Error('Invalid input');
    }

    // if is array, we'll clone the arr 
    // and fill the new array with placeholders
    const placeholders = [...arr];
    return placeholders.fill('?').join(', ').trim();
}


exports.multipleColumnSet = (object) => {
    if (typeof object !== 'object') {
        throw new Error('Invalid input');
    }

    const keys = Object.keys(object);
    const values = Object.values(object);


    columnSet = keys.map(key => `${key} = ?`).join(', ');

    return {
        columnSet,
        values
    }
}


exports.stagService = async (url, wscookie, params, school = null) => {
    if (!wscookie) {
        return;
        throw new HttpException(401, 'token is require');
    }

    let query = '';

    if (params && Object.values(params).length > 0) {
        query = `?${Object.keys(params).map(key => key + '=' + params[key]).join('&')}`;
    }

    let finalUrl = url;
    if (school && !url.startsWith('http')) {
        finalUrl = `${schoolsUrl[school]}${url}${query}`;
    } else {
        finalUrl = `${finalUrl}${query}`
    }

    const respond = await axios.get(finalUrl, {
        headers: {
            cookie: `WSCOOKIE=${wscookie}`
        }
    });

    return respond.data;
};

exports.generateUniqueFieldName = (originalFileName, fileType, targetDir) => {
    // Check if file already exists and create unique name
    let i = 1;
    let uniqueFileName = originalFileName + '-' + moment().format('YYYY-MM-DD');

    while (fs.existsSync(`${targetDir}${uniqueFileName}.${fileType}`)) {
        uniqueFileName = `${originalFileName}-${moment().format('YYYY-MM-DD')}(${i})`;
        i++;
    }

    return uniqueFileName;
}

exports.quote = value => `'${value}'`;

exports.uploadFile = (req, targetDir, fileKey, validation = null) => {
    if (!req.files || Object.keys(req.files).length === 0) {
        throw new HttpException(400, 'No files were uploaded.');
    }

    if (typeof validation === "function") {
        const isValid = validation(req);
        if (!isValid) {
            throw new HttpException(500, 'File is not valid!');
        }
    }

    let file = req.files[fileKey];
    const fullFile = file.name;
    let [fileName, fileType] = fullFile.split('.');
    const finalDirPath = dirname(require.main.filename) + targetDir;

    if (!fs.existsSync(finalDirPath)) {
        fs.mkdirSync(finalDirPath, {recursive: true});
    }

    fileName = this.generateUniqueFieldName(fileName, fileType, finalDirPath);

    file.mv(`${finalDirPath}${fileName}.${fileType}`, (err) => {
        if (err)
            throw new HttpException(500, err);
    });

    return {
        fileName,
        fileType
    }
}

exports.removeFile = (path) => {
    const fullPath = dirname(require.main.filename) + path;

    try {
        if (!fs.existsSync(fullPath)) {
            return true;
        }

        fs.unlinkSync(fullPath)
        return true;
    } catch(err) {
        console.error(err)
        return false;
    }
}

